<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Pitter | Login </title>

    <link href="/dashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/dashboard/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/dashboard/css/animate.css" rel="stylesheet">
    <link href="/dashboard/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

<div class="loginColumns animated fadeInDown">
    <div class="row">

        <div class="col-md-6">
            <h2 class="font-bold">Welcome to Pitter</h2>

            <p>
                With the recent addition of Laravel 5.3, we have took it upon ourselves to rewrite most of the core functions for stability and optimization.
                <br>
                <br>
                We hope with this crucial update that you find many improvements upon our platform and for features that are yet to come.
            </p>

        </div>
        <div class="col-md-6">
            <div class="ibox-content">
                <form class="m-t" role="form" action="/login" method="post">
                    {{ csrf_field() }}
                    @if(isset($error))
                        <p class="m-t">
                            <small style="color: red; ">
                                {{ $error }}
                            </small>
                        </p>
                    @endif
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email" required="" name="email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Password" required="" name="password">
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b">Login</button>


                    <p class="text-muted text-center">
                        <a href="/forgot">
                            <small>Forgot password?</small>
                        </a>
                        <br>
                        <small>Do not have an account?</small>
                    </p>
                    <a class="btn btn-sm btn-white btn-block" href="/register">Create an account</a>
                </form>

            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            Pitter, LLC
        </div>
        <div class="col-md-6 text-right">
            <small>© 2015-2017</small>
        </div>
    </div>
</div>

</body>

</html>
