<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Pitter | Register</title>

    <link href="/dashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/dashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/dashboard/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/dashboard/css/animate.css" rel="stylesheet">
    <link href="/dashboard/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name">
                <img src="/img/256x256.png" width="150" class="floating">
            </h1>

        </div>
        <h3>Uploading just got more awesome</h3>
        <p>Create account to start using our platform</p>
        <form class="m-t" role="form" action="/register" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="name" class="form-control" placeholder="Name" required="" name="name">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Email" required="" name="email">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" required="" name="password">
            </div>
            <div class="form-group">
                <div class="checkbox i-checks"><label> <input type="checkbox" required name="agree">
                        <i></i>
                        &nbsp; I agree to the <a href="https://pitter.us/terms">Terms of Service</a>
                    </label>
                </div>
            </div>
            @if(isset($error))
                <div class="alert alert-danger text-center">
                    {{ $error }}
                </div>
            @endif
            <button type="submit" class="btn btn-primary block full-width m-b">Register</button>

            <p class="text-muted text-center">
                <small>Already have an account?</small>
            </p>
            <a class="btn btn-sm btn-white btn-block" href="/login">Login</a>
        </form>
    </div>
</div>

<!-- Mainly scripts -->
<script src="/dashboard/js/jquery-3.1.1.min.js"></script>
<script src="/dashboard/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="/dashboard/js/plugins/iCheck/icheck.min.js"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });
</script>
</body>

</html>
