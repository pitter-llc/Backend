@extends("layouts.email")
@section("title", "Account Verified")
@section("content")
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-success">
            <div class="panel-heading text-center">
                Email Successfully Verified
            </div>
            <div class="panel-body text-center" style="height: 65vh; margin-top: 5%">
                <br><br>
                <span class="fa fa-check-circle fa-5x text-success"></span>
                <br>
                <h4>Account Verified</h4>
                <p>
                    Welcome to Pitter!
                    <br>
                    Thank you for verifying your Email Address.
                    <br><br>
                    <a href="http://panel. {{ env("DOMAIN", "localhost") }}">Click here to go to the panel</a>
                    <br><br><br><br>
                </p>
            </div>
        </div>
    </div>
@endsection