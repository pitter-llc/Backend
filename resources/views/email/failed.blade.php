@extends("layouts.email")
@section("title", "Verification Failed")
@section("content")
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-danger">
            <div class="panel-heading text-center">
                Email Verification Failure
            </div>
            <div class="panel-body text-center" style="height: 65vh; margin-top: 5%">
                <br><br>
                <span class="fa fa-times-circle fa-5x text-danger"></span>
                <br>
                <h4>We failed to verify your email address</h4>
                <p>
                    The link you may have clicked either expired or your account is already validated.
                    <br>
                    Sorry for the inconvenience.
                </p>
            </div>
        </div>
    </div>
@endsection