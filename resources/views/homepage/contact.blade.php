@extends('layouts.homepage')
@section('content')
    <!-- begin #contact -->
    <div id="contact" class="content bg-silver-lighter" data-scrollview="true">
        <!-- begin container -->
        <div class="container">
            <h2 class="content-title">Contact Us</h2>
            <p class="content-desc">
                Below is a form of which you can contact our support team directly.
                <br>
                Feel free to include any feedback or inquiry.
            </p>
            <!-- begin row -->
            <div class="row">
                <!-- begin col-10 -->
                <div class="col-md-10 col-md-offset form-col" data-animation="true" data-animation-type="fadeInRight">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name <span class="text-theme">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="Full Name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email <span class="text-theme">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="Email Address"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Message <span class="text-theme">*</span></label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="20" style="height: 200px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-9 text-left">
                                <button type="submit" class="btn btn-success btn-theme btn-block">Send Message</button>
                            </div>
                        </div>
                    </form>
                    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                </div>
                <!-- end col-6 -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end #contact -->
@stop
