@extends('layouts.homepage')
@section('content')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- begin #contact -->
    <div id="contact" class="content bg-silver-lighter" data-scrollview="true">
        <!-- begin container -->
        <div class="container">
            <h2 class="content-title">Abuse/DMCA Takedown</h2>
            <p class="content-desc">
                Please acknowedge that abuse reports are serious, and that false data will be discarded.
            </p>
            <!-- begin row -->
            <div class="row">
                <!-- begin col-10 -->
                <div class="col-md-10 col-md-offset form-col" data-animation="true" data-animation-type="fadeInRight">
                    <form class="form-horizontal" target="/abuse" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-3">Name <span class="text-theme">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="Your Name" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email <span class="text-theme">*</span></label>
                            <div class="col-md-9">
                                <input type="email" class="form-control" placeholder="your.name@company.com" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Telephone <span class="text-theme">*</span></label>
                            <div class="col-md-9">
                                <input type="number" class="form-control" placeholder="+1(000)-000-0000" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Mailing Address <span
                                        class="text-theme">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="PO Box 4668, New York, NY 10163"
                                       required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Material in Question <span
                                        class="text-theme">*</span></label>
                            <div class="col-md-9">
                                <input type="url" class="form-control" placeholder="https://i.pitter.us/example.png"
                                       required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Message <span class="text-theme">*</span></label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="10"
                                          placeholder="Please enter a formal and descriptive message about the issue of the material in question, with evidence that the content is infringing your intellectual property."
                                          required style="height: 200px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-9 text-left panel-body">
                                <button type="submit" class="btn btn-success btn-theme btn-block">File Complaint</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- end col-6 -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end #contact -->
@stop
