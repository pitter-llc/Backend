@extends('layouts.homepage')
@section('content')
    <!-- begin #contact -->
    <div id="contact" class="content bg-silver-lighter" data-scrollview="true">
        <!-- begin container -->
        <div class="container">
            <h2 class="content-title">Cookie Policy</h2>
            <p class="content-desc">
            <h4>What is the cookie law?</h4>
            The Cookie Law is a piece of privacy legislation that requires websites to get consent from visitors or to
            let them know that data will be stored on their computer.
            <br><br><br>
            <h4>What data is being stored?</h4>
            As bad as the above sounds, nothing more than session data. <br>
            Cookies in this instance store a unique identifier of who you are that will communicate with our server and
            your browser to determine who and where you're logged into.
            <br><br><br>
            <h4>How big are these cookies?</h4>
            While the process of cookies is automated for the server and the browser that you're on, we expect these
            cookies to be no bigger than 100KB.
            <hr>
            <h4>The Agreement</h4>
            Upon registration or login to our platform, you acknowedge upon your behalf that you accept the usage of
            cookies to be utilized for your current session.<Br>
            Upon closing your browser the cookies stored for that browsing session will be deemed invalid, and no longer
            usable and are considered to be deleted.
            </p>
        </div>
        <!-- end container -->
    </div>
    <!-- end #contact -->
@stop
