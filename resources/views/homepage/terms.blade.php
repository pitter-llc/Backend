@extends('layouts.homepage')
@section('content')
    <!-- begin #contact -->
    <div id="contact" class="content bg-silver-lighter" data-scrollview="true">
        <!-- begin container -->
        <div class="container">
            <h2 class="content-title">Terms of Service</h2>
            <p class="content-desc">
            <h4>Introduction</h4>
            The terms on this page is provided to you to establish an agreement as a set of guidelines while using our
            service.
            <br><br><br>
            <h4>Eligibility and Registration</h4>
            Pitter is a public service that anyone of the age of legal age of consent in the user's residing country may
            use. From the moment you create an account you effectively state that you are agreeing that you are to
            be bound to these terms until the termination of your account either by us or yourself.


            <br><br><br>
            <h4>Uploads</h4>
            The uploader retains all rights to any and all of the uploader’s data. Pitter shall
            not own or license any data, content, information or material that you or your Users include in Uploaded
            Data and submit to the Service. Pitter will not monitor, edit or disclose any information regarding
            You or your account, including any Uploaded Data, without your prior permission, except as
            permitted by this Agreement or as required by applicable law. Pitter may access your account,
            including Uploaded Data, to respond to service or technical problems or as stated in this Agreement or
            required by applicable law. You, not Pitter, shall have sole responsibility for the accuracy,
            quality, integrity, legality, reliability, appropriateness and copyright of all Uploaded Data, and
            Pitter shall not be responsible or liable for the deletion, correction, destruction, damage, loss or
            failure to store any Uploaded Data.

            <br><br><br>
            <h4>Complaints and Removal Policy</h4>
            Pitter reserves the right to delete or disable the accounts of Users who we believe to be infringing
            the intellectual property rights of others and to remove any such infringing materials. If you believe the
            Services have been used in a way that constitutes copyright infringement, please send a message via our
            abuse form, providing all of the following information, as required by the Digital Millennium Copyright Act:
            <ul>
                <li>
                    A link to the infringing file
                </li>
                <li>
                    A statement that you have identified content on the Service that infringes a copyright you own or
                    the
                    copyright of a third party for whom you are authorized to act
                </li>
                <li>
                    A description of the copyrighted work you claim has been infringed
                </li>
                <li>
                    A specific description of where the allegedly infringing material is located on the Services,
                    including a
                    URL or exact description of the content's location
                </li>
                <li>
                    Your Full Name, Address, Telephone Number, and Email Address
                </li>
                <li>
                    A statement that you have a good faith belief that the disputed use of the copyrighted material is
                    not
                    authorized by the copyright owner, its agent, or the law (e.g., as a fair use)
                </li>
                <li>
                    A statement that, under penalty of perjury, the information in your notice is accurate and that you
                    are
                    authorized to act on behalf of the owner of the exclusive right that is allegedly infringed
                </li>
                <li>
                    Your electronic signature
                </li>
            </ul>
            Claims can be sent to us from the <a href="/abuse">abuse</a> page.
            <br>
            Pitter also reserves the right to forward the information in the copyright-infringement notice to
            the User who allegedly provided the infringing content.


            <br><br><br>
            <h4>Modifications to this Agreement</h4>
            Pitter reserves the right at it's sole discretion to change, modify, ammend or alter this agreement at any
            given time without user consent.<br>
            The most recent version of this agreement can always be found on this page, or any providing link marked
            with "Terms of Service" on this domain.
            </p>
        </div>
        <!-- end container -->
    </div>
    <!-- end #contact -->
@stop
