@extends('layouts.homepage')
@section('content')
    <!-- begin #contact -->
    <div id="contact" class="content bg-silver-lighter" data-scrollview="true">
        <!-- begin container -->
        <div class="container">
            <h2 class="content-title">Data Policy</h2>
            <p class="content-desc">
            <h4>What is the data policy?</h4>
            The data policy is an agreement between us and the registered user of our network that states that the user
            acknowledges how their data will be utilized and how data is stored.
            <br><br><br>
            <h4>Is my data shared?</h4>
            Privacy is a core fundamental of Pitter, and we will not share your <u>private or personal</u> data such as
            <u>account information</u> to any third party under any circumstance.
            <br>
            Any form of data uploaded to our platform is returned with a absolute link to the file uploaded, provided
            through the interface created by our applications. The file may only be accessed by anyone who has been
            granted the personal link to that individual file.
            <br><br><br>
            <h4>The right to be forgotten</h4>
            Pitter grants the user at any time to permanently discard their account and any form of associated data on
            our platform - This data includes <u>Logs, Uploaded files, Name, and Account Status</u>.
            <br>
            At any time of the state of your account becoming suspended for violation of our terms of service, the right
            to be forgotten will be revoked for the sole purpose of keeping track of infringement records, however your
            personal files will be deleted.
            <br><br><br>

            </p>
        </div>
        <!-- end container -->
    </div>
    <!-- end #contact -->
@stop
