@extends('layouts.homepage')
@section('content')
    <!--Navbar-->
    <nav class="navbar navbar-dark navbar-fixed-top scrolling-navbar">

        <!-- Collapse button-->
        <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#collapseEx">
            <i class="fa fa-bars"></i>
        </button>

        <div class="container">

            <!--Collapse content-->
            <div class="collapse navbar-toggleable-xs" id="collapseEx">
                <!--Navbar Brand-->
                <a class="navbar-brand" href="/">Pitter</a>
                <!--Links-->
                <ul class="nav navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#platform">Platform</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#stats">Statistics</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#team">Team</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav float-xs-right">
                    <li class="nav-item">
                        <a class="nav-link" href="https://panel.pitter.us/">Panel</a>
                    </li>
                </ul>


            </div>
            <!--/.Collapse content-->
        </div>

    </nav>
    <!--/.Navbar-->

    <!--Mask-->
    <div class="view-main hm-black-strong">
        <div class="full-bg-img flex-center">
            <ul>
                <li>
                    <h1 class="h1-responsive wow fadeInDown" data-wow-delay="0.4s">Simple screenshot and file uploading</h1></li>
                </li>
                <li>
                    <p class="wow fadeInDown" data-wow-delay="0.4s">
                        Our goal is to make the process simple, while providing a bunch of insight and statistics
                    </p>
                </li>
                <li>
                    <a href="https://panel.pitter.us/register" class="btn btn-primary waves-effect waves-light wow fadeInLeft" data-wow-delay="0.4s">
                        Sign Up
                    </a>
                    <a href="https://download.pitter.us/pitter-installer.exe" class="btn btn-success waves-effect waves-light wow fadeInRight"
                       data-wow-delay="0.4s">
                        Download
                    </a>
                </li>
            </ul>
        </div>

    </div>
    <!--/.Mask-->

    <!-- begin #platform -->
    <section id="platform">
        <div class="content" data-scrollview="true">
            <!-- begin container -->
            <div class="container">
                <h2 class="content-title">Our Platform</h2>
                <p class="content-desc">
                    Here's some core ideals and features we've implemented on our platform
                </p>
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i
                                        class="fa fa-check"></i></div>
                            <div class="info">
                                <h4 class="title">Easy to Use</h4>
                                <p class="desc">We created our panel for everyone. If you enjoy statistics, or just want
                                    a platform to upload screenshots and files &#8212; Pitter is for you.</p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn">
                                <i class="fa fa-user-secret"></i>
                            </div>
                            <div class="info">
                                <h4 class="title">Privacy in mind</h4>
                                <p class="desc">
                                    All files and internal network data is AES-256-CBC encrypted. Any form of
                                    information that you provide is private.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn">
                                <i class="fa fa-dashboard"></i>
                            </div>
                            <div class="info">
                                <h4 class="title">Performance Engineered</h4>
                                <p class="desc">
                                    All our data is stored on solid state drives to provide the lowest read latency possible when downloading files from any of
                                    our servers.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                </div>
                <!-- end row -->
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i
                                        class="fa fa-upload"></i></div>
                            <div class="info">
                                <h4 class="title">Almost No Limits</h4>
                                <p class="desc">We support a maximum upload size of 100MB per file. As long as you're
                                    within
                                    the Terms of Service you can upload as much as you want.</p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->

                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i
                                        class="fa fa-globe"></i></div>
                            <div class="info">
                                <h4 class="title">Anycast Network</h4>
                                <p class="desc">
                                    Our network is behind CloudFlare's CDN. You will always be directed to the fastest available server.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i
                                        class="fa fa-area-chart"></i></div>
                            <div class="info">
                                <h4 class="title">Statistics Powered</h4>
                                <p class="desc">Our statistics engine provide data and changes in real time. This allows
                                    you
                                    to see how your uploaded files perform in real time.</p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                </div>
                <!-- end row -->

                <!-- begin row -->
                <div class="row">
                    <!-- begin col-4 -->
                    <div class="col-md-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i
                                        class="fa fa-refresh"></i></div>
                            <div class="info">
                                <h4 class="title">Client Synchronization</h4>
                                <p class="desc">
                                    Pitter synchronizes all it's data with the cloud. Your devices will always be paired together to access your
                                    files at any location.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i
                                        class="fa fa-code-fork"></i></div>
                            <div class="info">
                                <h4 class="title">Programming Interface</h4>
                                <p class="desc">
                                    We're in the process of creating an extensive API to allow developers to hook into our platform to create a simple storage
                                    powered backend.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i
                                        class="fa fa-circle-o"></i></div>
                            <div class="info">
                                <h4 class="title">Transparency</h4>
                                <p class="desc">
                                    We're open and honest - If any critical events occur on the network you will be informed.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                </div>
                <!-- end row -->


            </div>
            <!-- end container -->
        </div>
    </section>
    <!-- end #platform -->

    <!-- begin #milestone -->
    <section id="stats">
        <div class="view-milestone">
            <div id="milestone" class="content full-bg-image view-overlay" data-scrollview="true">
                <!-- begin container -->
                <div class="container-fluid " data-animation="true" data-animation-type="fadeInLeft">
                    <!-- begin row -->
                    <div class="row">

                        <!-- begin col-3 -->
                        <div class="col-md-2 col-sm-2 milestone-col">
                            <div class="milestone">
                                <div class="number" data-animation="true" data-animation-type="number"
                                     data-final-number="{{ $user_count }}" id="members">
                                    {{ number_format($user_count) }}
                                </div>
                                <div class="title">Users</div>
                            </div>
                        </div>
                        <!-- end col-3 -->
                        <!-- begin col-3 -->
                        <div class="col-md-2 col-sm-2 milestone-col">
                            <div class="milestone">
                                <div class="number" data-animation="true" data-animation-type="number"
                                     data-final-number="{{ $files_uploaded }}" id="files">
                                    {{ number_format($files_uploaded) }}
                                </div>
                                <div class="title">Uploads</div>
                            </div>
                        </div>
                        <!-- end col-3 -->
                        <!-- begin col-3 -->
                        <div class="col-md-2 col-sm-2 milestone-col">
                            <div class="milestone">
                                <div class="number" data-animation="true" data-animation-type="number"
                                     data-final-number="{{ $requests }}" id="served">
                                    {{ number_format($requests) }}
                                </div>
                                <div class="title">Requests</div>
                            </div>
                        </div>
                        <!-- end col-3 -->
                        <!-- begin col-3 -->
                        <div class="col-md-2 col-sm-2 milestone-col">
                            <div class="milestone">
                                <div class="number">
                                <span class="" style="display: inline" data-animation="true"
                                      data-animation-type="float"
                                      data-final-number="{{  number_format($bandwidth, 3) }}" id="bandwidth">
                                    {{ number_format($bandwidth, 3) }}
                                </span>
                                    GB
                                </div>
                                <div class="title">Bandwidth Consumed</div>
                            </div>
                        </div>
                        <!-- end col-3 -->
                        <!-- begin col-3 -->
                        <div class="col-md-2 col-sm-2 milestone-col">
                            <div class="milestone">
                            <span>
                                <div class="number">
                                    <span class="" style="display: inline" data-animation="true"
                                          data-animation-type="float"
                                          data-final-number="{{ number_format($space_consumed, 3) }}" id="disk">
                                    {{ number_format($space_consumed, 3) }}
                                </span>
                                    GB
                                </div>
                            </span>
                                <div class="title">Space Consumed</div>
                            </div>
                        </div>
                        <!-- end col-3 -->
                        <!-- begin col-3 -->
                        <div class="col-md-2 col-sm-2 milestone-col">
                            <div class="milestone">
                                <div class="number" data-animation="true" data-animation-type="number"
                                     data-final-number="{{ $server_count }}" id="servers">
                                    {{ number_format($server_count) }}
                                </div>
                                <div class="title">Servers</div>
                            </div>
                        </div>
                        <!-- end col-3 -->
                    </div>
                    <!-- end row -->
                </div>
                <!-- end container -->
            </div>
        </div>
    </section>
    <!-- end #milestone -->

    <!-- begin #team -->
    <section id="team">
        <div id="team" class="content" data-scrollview="true">
            <!-- begin container -->
            <div class="container">
                <h2 class="content-title">Our Team</h2>
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <!-- begin team -->
                        <div class="team">
                            <div class="image" data-animation="true" data-animation-type="flipInX">
                                <img src="/img/staff/andrew.jpg" width="128"/>
                            </div>
                            <div class="info">
                                <h3 class="name">Andrew Haughie</h3>
                                <div class="title text-theme a-alt">SYSADMIN</div>
                                <div class="social">
                                    <a href="https://elyc.in/"><i class="fa fa-globe fa-lg fa-fw"></i></a>
                                    <a href="https://twitter.com/iamelycin">
                                        <i class="fa fa-twitter fa-lg fa-fw"></i>
                                    </a>
                                    <a href="https://github.com/elycin">
                                        <i class="fa fa-github fa-lg fa-fw"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- end team -->
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <!-- begin team -->
                        <div class="team">
                            <div class="image" data-animation="true" data-animation-type="flipInX">
                                <img src="/img/staff/tyler.png"/>
                            </div>
                            <div class="info">
                                <h3 class="name">Tyler Corsair</h3>
                                <div class="title text-theme a-alt">DEVELOPER</div>
                                <div class="social">
                                    <a href="https://thebluecorsair.com/">
                                        <i class="fa fa-globe fa-lg fa-fw"></i>
                                    </a>
                                    <a href="https://twitter.com/thebluecorsair">
                                        <i class="fa fa-twitter fa-lg fa-fw"></i>
                                    </a>
                                    <a href="https://github.com/crr">
                                        <i class="fa fa-github fa-lg fa-fw"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- end team -->
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <!-- begin team -->
                        <div class="team">
                            <div class="image" data-animation="true" data-animation-type="flipInX">
                                <img src="/img/staff/ole.jpg" width="128"
                                     height="128"/>
                            </div>
                            <div class="info">
                                <h3 class="name">Ole Kristian Sandum</h3>
                                <div class="title text-theme a-alt">DEVELOPER</div>
                                <br>
                                <div class="social">
                                    <a href="https://twitter.com/yeoleole">
                                        <i class="fa fa-twitter fa-lg fa-fw"></i>
                                    </a>
                                    <a href="https://github.com/rocoty">
                                        <i class="fa fa-github fa-lg fa-fw"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- end team -->
                    </div>
                    <!-- end col-4 -->

                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
    </section>
    <!-- end #team -->
@stop

@section('js')
    <script>
        $(document).ready(function () {
            setInterval(function () {
                $.getJSON("/statistics", function (data) {
                    $("#members").html(data.user_count.toLocaleString());
                    $("#files").html(data.files_uploaded.toLocaleString());
                    $("#disk").html(data.space_consumed.toLocaleString());
                    $("#served").html(data.requests.toLocaleString());
                    $("#bandwidth").html(data.bandwidth.toLocaleString());
                    $("#servers").html(data.server_count.toLocaleString());
                });
            }, 5000);
        });
    </script>
@stop