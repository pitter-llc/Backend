@extends('layouts.panel')
@section("title", "Workshop")
@section("body")
    <br>
    <div class="panel panel-default">
        <div class="panel-body">
            <h2>Welcome to the Workshop</h2>
            The workshop is our area of pitter of which features are either being tested, or just for listing other applications that integrate into our platform that you
            may use.
            <br><br>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <h2>Official Releases</h2>
            Software under this pane is created by the official pitter team, but however support may not be provided by us as these applications are either
            experimental or deprecated.
            <hr>
            <table class="table table-responsive table-bordered">
                <thead>
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Description
                    </th>
                    <th>
                        Operating Systems
                    </th>
                    <th>
                        Version
                    </th>
                    <th>
                        Download
                    </th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <h2>Community Releases</h2>
            These releases are projects that are maintained by the community, or other developers that use Pitter.
            <br>
            Under any circumstance these downloads are not covered under any warranty or provide any support unless otherwise specified by the developer.
            <hr>

            <table class="table table-responsive table-bordered">
                <thead>
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Description
                    </th>
                    <th>
                        Operating Systems
                    </th>
                    <th>
                        Developer
                    </th>
                    <th>
                        Version
                    </th>
                    <th>
                        Download
                    </th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

@endsection