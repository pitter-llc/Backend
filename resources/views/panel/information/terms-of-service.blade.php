@extends("layouts.panel")
@section("title", "Terms of Service")
@section("body")
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2>Terms of Service</h2>
                    This page is provided as general information to explain to the user legal guidelines of using this service.
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>LEGAL INTRODUCTION</h3>
                    <hr>
                    Pitter, LLC Incliding specifically Pitter and any future subsidaries, affiliates or division and all other data sources which may be expressed
                    collectively as ("we", "ourselves", "maintainers", "support", and "developers") welcome you to our project. We may at any time throughout this
                    document refer to the user as one of the following: ("user", "subscriber", "premium member", and member") and refer to Pitter as ("the platform",
                    "system", "network(s)", and "service").
                    <br><br>
                    We ask that you please read these terms carefully before further continuing to utilize our service, as these terms apply to all users of the services
                    provided throughout the domain "pitter.us". If you are utilizing the services on behalf of a legal registered entity, organization, company you
                    warrant that you represent that you have the authority to make administrative decisions of the company to bind the organization to these terms of
                    services, and acknowedge that agreement to these terms initiated after account registration.
                    <br><br>
                    These terms of services provide to the user that all disputes between you and Pitter, LLC will be resolved by Binding Arbitration, and legally
                    acknowledged that you agree to wave your right to go to court to assert of defend any legal representation except for damages that may be taken to
                    small claims court.
                    Your rights are to be determined by a neutral arbitrator and not a judge or jury, and that any uploaded material cannot be brought as a class action.
                    <br><br>
                    We reserve the right at any time under the curcumstance for any reason to amend or modify this legal document without prior or future notice to the
                    user, provided that if any such alterations constitute a material change that we will notify the user by website notification. Any future amendments
                    or modifications to this document shall take immediate effect when posted, and you agree to be bound by revised versions of this document under the
                    continuation of using the service.
                </div>
            </div>
        </div>
        <div class="col-md-5 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Explanation</h3>
                    <hr>
                    The legal details you are reading to the left is an introductions statement that is legally required. We use this to identify ourselves and provide a
                    base guideline to the user while reading this legal document.
                    <br><br>
                    You are expected to read this document, and acknowledge the terms to use our service of which identifies the regions and bounds between the developers
                    and you. It is a basic legal explanation that you under any circumstance cannot file a lawsuit for any possible damage that happens to your uploaded
                    content, and that you are to be held responsible for anything you provide to our service.

                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>LIABILITY AND ACCURACY</h3>
                    <hr>
                    In any event, we shall not under any circumstance be liable for damages caused by you in any direct, indirect, incidental, special or other forms of
                    physical or electronic damages to either the law or intelectual property copyright holders. In the event of a valid abuse complaint of infringing
                    copyright material or other illicit content, we reserve the right to suspend your account and prevent further usage on our network. We under any
                    circumstance faithfully maintain a good security practice in terms of accuracy, however the user who has uploaded files to their account is
                    responsible for full accuracy of any file under their account as we are just a service provider in a legal sense.
                </div>
            </div>
        </div>
        <div class="col-md-5 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Explanation</h3>
                    <hr>
                    Please remember that users are to be held responsible for their uploads, and any links they provide to anyone that are from "pitter.us". We cannot be
                    held liable for anything on behalf of sole user interaction, but however is legally obligated will remove the file from our network and likely suspend
                    the account.

                    <br><br>
                    But, this is an explanation stating that we cannot verify any form of expected results or accuracy of any content on our network.
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>PRIVACY, ENCRYPTION AND PROTECTION</h3>
                    <hr>
                    Pitter is operated out of europe in various locations. In regards to <a href="https://en.wikipedia.org/wiki/Data_Protection_Act_1998">
                        The Data Protection Act 1998 (DPA)
                    </a>
                    we grand users the ability to be forgotten given proper consent.
                    Under this act, the user will be able to erase all information contained in our databases by their choice.
                    <br><br>
                    The user also understands that it is in our best intentions to maintain confidentiality of uploads in the extent of legal usage of his, her or their
                    uploaded files, and that encryption is optional but however when specified will be encrypted using the algorithm known as AES-256-CBC.
                    <br><br>
                    The user agrees that their uploaded files in the processing stage of which they are stored on storage mediums, that their file is subject to being
                    scanned for malware, and that an calculated checksum will be compared with a database to combat piracy in good faith to maintain quality of service
                    for the network.
                </div>
            </div>
        </div>
        <div class="col-md-5 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Explanation</h3>
                    <hr>
                    We understand how much data matters to anyone, and privacy in the same regard.
                    <br>
                    Pitter under any circumstance grants the user the right to be forgotten, and have any trace of their fingerprint on our servers to be permanently
                    deleted in compliance with the law.
                    <br><br>
                    Pitter grants the user the ability to have their uploads encrypted by passing an "encrypt" flag with the upload request to ensure that when files are
                    stored that there is no interference between service providers.
                    <br><br>
                    For protection of our users and everyone else on the internet clicking links, we scan all uploads immediately with ClamAV to filter out malware.
                    <br>
                    We're also working on an anti-piracy mechanism of which publishers can prevent their property from being distributed on our networks.
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>DENIAL OF SERVICE AND AUTOMATION</h3>
                    <hr>
                    Pitter under any circumstance reserves the right to deactivate or terminate an account under the belief of good intentions for the rest of the
                    network, in the events that we determine a abuse complaint to be valid of which infringes upon the rights of someone else or doesn't comply with local
                    legislature.
                    <br><br>
                    The user acknowledges that pitter is also an automated service of which actions are carried out automatically.
                    Uploads published to our servers that are associated with a user account that is labeled as free, or a non paying user will have their files retained
                    for a maximum period of 3 months, where as a paid or premium member will have their files retained for a year.
                </div>
            </div>
        </div>
        <div class="col-md-5 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Explanation</h3>
                    <hr>
                    This is a basic description stating the events of which we may delete files from our servers, along with a brief explanation of how our automation
                    works.
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>PAID SERVICES</h3>
                    <hr>
                    Pitter offers a premium account status of which attributes are modified for your file uploads, to the extent that your uploads are retained for a year
                    rather than the free account limit of three months. The user should also acknowledge that we do not offer refunds.
                </div>
            </div>
        </div>
        <div class="col-md-5 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Explanation</h3>
                    <hr>
                    In the event that we offer paid services, under any circumstance you will not be issued a refund.
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>WORKSHOP</h3>
                    <hr>
                    The workshop is a community object of which other users of pitter can design and submit applications for others to use. The user acknowledges that
                    pitter is not responsible directly for any projects made by the user, as other users are utilizing our application programming interface.
                    <br><br>
                    The user should take notice that any project downloaded from the workshop is also not hosted on our network, thus we have no control over the design
                    or distribution of the project that the said developer has made.
                </div>
            </div>
        </div>
        <div class="col-md-5 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Explanation</h3>
                    <hr>
                    The workshop is created and for the most part controlled by community input. <br>
                    Please be careful of the projects that you check out.
                </div>
            </div>
        </div>
    </div>
@endsection