@extends('layouts.panel')
@section("title", "Uploads")
@section("scripts")
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $(".s-ta:first").addClass("active");
            $(".tab-pane:first").addClass("active");
        });
    </script>
@endsection
@section('body')
    <!--Main layout-->
    <main class="">
        <div class="row">
            <br>
            <div class="col-md-12">
            @if(!empty(array_filter($categories)))
                <!-- Nav tabs -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <ul class="nav nav-tabs md-pills pills-default" role="tablist">
                                @foreach($categories as $name => $category)
                                    <li class="nav-item">
                                        <a class="s-ta nav-link waves-light" data-toggle="tab" href="#{{ str_replace(" ","-",strtolower($name)) }}-panel"
                                           role="tab">{{ $name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                            <br>
                            <!-- Tab panels -->

                            <div class="tab-content card">
                            @foreach($categories as $name => $category)
                                <!--Panel {{ $name }}-->
                                    <div class="tab-pane fade in" id="{{ str_replace(" ","-",strtolower($name)) }}-panel" role="tabpanel">
                                        @if($name != "Images")
                                            <table class="footable table table-striped table-responsive toggle-arrow-tiny default breakpoint">
                                                <thead>
                                                <tr>
                                                    <!-- Visible -->
                                                    <th class="footable-visible footable-first-column footable-sortable">Filename<span
                                                                class="footable-sort-indicator"></span></th>
                                                    <th class="footable-visible footable-sortable">Original Filename<span class="footable-sort-indicator"></span></th>
                                                    <th class="footable-visible footable-sortable">Filesize<span class="footable-sort-indicator"></span></th>
                                                    <th class="footable-visible footable-sortable">Views<span class="footable-sort-indicator"></span></th>
                                                    <th class="footable-visible footable-sortable">Bandwidth<span class="footable-sort-indicator"></span></th>
                                                    <th class="footable-visible footable-last-column footable-sortable">Date Uploaded<span
                                                                class="footable-sort-indicator"></span></th>
                                                    <!-- /Visible -->

                                                    <!-- Collapsed -->
                                                    <th data-hide="all" class="footable-sortable" style="display: none;">Encrypted Filesize<span
                                                                class="footable-sort-indicator"></span></th>
                                                    <th data-hide="all" class="footable-sortable" style="display: none;">Last Viewed<span
                                                                class="footable-sort-indicator"></span></th>
                                                    <th data-hide="all" class="footable-sortable" style="display: none;">Mountpoint<span
                                                                class="footable-sort-indicator"></span></th>
                                                    <!-- /Collapsed -->
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($category as $item)
                                                    <tr class="footable-even" style="">
                                                        <td class="footable-visible footable-first-column">
                                                            <span class="footable-toggle"></span>
                                                            <a href="https://i.pitter.us/{{ $item->filename }}" target="_blank">{{ $item->filename }}</a>
                                                        </td>
                                                        <td class="footable-visible">{{ $item->original_filename }}</td>
                                                        <td class="footable-visible">{{ number_format($item->size/1024/1024,3) }} MiB</td>
                                                        <td class="footable-visible">{{ $item->views }}</td>
                                                        <td class="footable-visible">{{ number_format($item->views * $item->size/1024/1024/1024,4) }} GiB</td>
                                                        <td class="footable-visible footable-last-column">
                                                            {{ $item->created_at }}
                                                        </td>
                                                        <td style="display: none;">{{ number_format($item->encrypted_size/1024/1024,3) }} MiB</td>
                                                        <td style="display: none;">
                                                            {{ $item->updated_at }}
                                                        </td>
                                                        <td style="display: none;">Server {{ $item->mount }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            @foreach($category as $item)
                                                <a href="https://i.pitter.us/{{ $item->filename }}" target="_blank">
                                                    <img src="https://i.pitter.us/thumb/{{ $item->filename }}" style="display: inline !important;"/>
                                                </a>
                                            @endforeach
                                        @endif
                                    </div>
                                <!--/.Panel {{ $name }}-->
                                @endforeach
                            </div>
                        </div>
                    </div>

                @else
                    <div class="panel panel-default">
                        <div class="panel-body text-center">
                            <h2>
                                <i class="fa fa-4x fa-upload"></i>
                                <br><br>
                                You haven't uploaded anything yet!
                            </h2>
                            In order for pitter to start categorizing your uploads, you first need to have at least one!
                            <br>
                            Or you can drop something here to start.
                            <br><br><br>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-body text-center">
                            <h2>
                                <span class="fa fa-4x fa-rocket"></span>
                                <br><br>
                                Get an app!
                            </h2>
                            Other than using the browser to upload files, you can also check out the <a href="/workshop">workshop</a> to find an application that you can
                            use to capture your desktop!
                            <br><br><br>
                        </div>
                    </div>
            @endif

            <!-- /.Live preview -->
            </div>
        </div>
    </main>
@endsection