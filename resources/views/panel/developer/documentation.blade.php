@extends("layouts.panel")
@section("title", "Developer Documentation")
@section("body")
    <div class="row  border-bottom white-bg dashboard-header">
        <h2>Developer Documentation</h2>
        The purpose of this page is to provide documentation of all available functions and routes that can be used to make your own library, or utilize for your
        applications.
        <br><br>
    </div>

    <div class="row border-bottom white-bg dashboard-header">
        <h2>Table of Contents</h2>
        <ul>
            <li><a href="#section-introduction">Introduction</a></li>
            <li>
                <a href="#section-system-structure">System Structure</a>
                <ul>
                    <li>
                        <a href="#section-system-structure-networking">Networking</a>
                        <ul>
                            <li><a href="#section-system-structure-networking-server-to-server">Server-to-Server Communication</a></li>
                            <li><a href="#section-system-structure-networking-locations">Server Locations</a></li>
                        </ul>
                    </li>
                    <li><a href="#section-system-structure-storage">Storage</a></li>
                    <li><a href="#section-system-structure-virus-scanning">Virus Scanning</a></li>
                </ul>
            </li>
            <li>
                <a href="#section-routes">Routes</a>
                <ul>
                    <li>
                        User
                        <ul>
                            <li>Authentication</li>
                            <li>Statistics</li>
                        </ul>
                    </li>
                    <li>
                        Uploads
                        <ul>
                            <li>Uploading a file</li>
                            <li>Deleting a file</li>
                            <li>Thumbnail Generation</li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>

    <div class="row border-bottom white-bg dashboard-header">
        <section id="section-introduction"></section>
        <h2>Introduction</h2>
        This section of the documentation is to explain to the user about things that should always be kept in mind when utilizing the API.
        <hr>
        <ul>
            <li>Each application should have it's own Application Key and Secret</li>
            <li>Every POST request over HTTPS should always contain the API Key and Secret</li>
            <li>At any time, the user may choose to deny access to the application after logging in</li>
            <li>Each API key is limited to <b>100,000</b> requests per day - If your account is premium, this limit is removed</li>
            <li>If the server is overloaded, your request will be queued and will hang until system resources are available</li>
        </ul>
    </div>

    <div class="row border-bottom white-bg dashboard-header">
        <section id="section-system-structure"></section>
        <h2>System Structure</h2>
        This section of the documentation serves to explain to you about how our system is designed, and why these choices have been made.
        <br><br>
        <b>Disclaimer:</b> Most of this information we have gathered to build our system is documented on this page for the purpose of showing others how far we've come
        and what we found works. Being that most of Pitter consists of open source software, you may feel free to utilize any of the methods provided in this section.

        <hr>
        <section id="section-system-structure-networking"></section>
        <h3>Networking</h3>
        During the starting phase of pitter, we've performed extensive background research into networks and considered numberous options on how we could deliver
        data over the internet as fast and reliable as possible on a low budget.<br>
        Starting our from a single server, we realized that using a single peering point to serve across the globe was a major issue, even with a high capasity provider
        like <a href="https://ovh.net">OVH</a>.
        <br>
        With the flexibility that <a href="https://cloudflare.net">Cloudflare</a> offered, it made sense that we could pull of a few features that would be costly in the
        real world if operated privately, such as the following:
        <ul>
            <li>Over 70+ regions for content delivery</li>
            <li>Denial of service protection</li>
            <li>Anycast DNS</li>
        </ul>

        Using Cloudflare's service has effectively reduced the system load, and even the bandwidth that comes out of a single server. Thanks to the Anycast DNS, we can
        also effectively scale up other webservers that work off of the same database to balance the system load between multiple machines.

        <br><br><br>

        <section id="section-system-structure-networking-server-to-server"></section>
        <h4>Server-to-Server Communication</h4>
        For communicating on the internet across regions while through an encrpted tunnel, we utilize a peer to peer VPN system known as
        <a href="https://en.wikipedia.org/wiki/Tinc_(protocol)">Tinc</a>.

        <br><br><br>

        <section id="section-system-structure-networking-locations"></section>
        <h4>Server Locations</h4>
        We understand that latency is a key factor of content delivery; As explained above about Cloudflare: Their network helps a bunch on cutting this number down.
        <br>
        Due to the reverse operation of the network, not only does the anycast work for you when accessing a cloudflare entry-point, but Cloudflare's server will also
        access the nearest server to you that has a copy of the file.
        <br><br>
        While our root server is currently stored in <b>Roubaix, France</b>, We've taken measurements from various areas to show the effective latency of operations:
        <ul>
            <li>
                Saint Lucie County, Florida
                <ul>
                    <li><b>Client to Cloudflare's Network:</b> 39.1 milliseconds</li>
                    <li><b>Server to Cloudflare's Network:</b> 0.086 milliseconds</li>
                    <li><b>Latency from server to client:</b> 136.6 milliseconds</li>
                    <li>
                        Using the data gathered above, we can determine that Cloudflare completes the delivery from the United States to Europe in under 96.6
                        milliseconds, or just under 1/10th of a second.
                    </li>
                </ul>
            </li>
        </ul>
        But to get back on the subject: We're hoping to place more servers in strategic locations in the future to achieve a goal of the lowest latency possible.

        <br><br>
        <hr>
        <br>

        <section id="section-system-structure-storage"></section>
        <h3>Storage</h3>
        Storage is a subject that we've put a lot of effort into for the purpose of learnign and seeing what would work best for us, such as writing our own distribution
        systems that have failed. (eg: Scalar)
        <br><br>
        The current system we have landed on consists of mounting <a href="">NFS Shares</a> as a filesystem over Tinc (explained in the networking section).
        <br>
        The benefit of this mounting mechanism with the <a href="https://en.wikipedia.org/wiki/Filesystem_in_Userspace">FUSE Kernel Module</a> is that we can take
        statistics on these machines remotely from whichever webserver you are connected to, such as the following:
        <ul>
            <li>Disk Attributes</li>
            <li>Free Space</li>
            <li>Used Space</li>
            <li>Total Space</li>
            <li>Partitions</li>
        </ul>

        <br>
        <hr>
        <br>

        <section id="section-system-structure-virus-scanning"></section>
        <h3>Virus Scanning</h3>
        For making sure that our network is safe for people to download from, we have a slightly overly-complex solution powered by
        <a href="https://www.clamav.net/">ClamAV</a> that we utilize.
        <br>
        This section will cover the process and explanation of how ClamAV works, and explain in depth of how our backend processes it.
        <br><br>
        <ul>
            <li>
                Basics of RAM
                <ul>
                    <li>
                        By the structural design of RAM, you have NAND chips that serve the sole purpose of holding temporary values, that is accessed randomly.
                    </li>
                    <li>
                        This method of storing data is temporary, or volatile.
                    </li>
                    <li>
                        Using RAM as a storage device can result in speeds anywhere from 5 GB/s to 11.7 GB/s
                    </li>
                </ul>
            </li>
            <li>
                How ClamAV works and utilizes RAM
                <ul>
                    <li>
                        <a href="https://help.ubuntu.com/community/ClamAV#Update_Virus_Definitions">freshclam</a> occasionally runs and updates the virus database.
                    </li>
                    <li>
                        The newly updated definitions are written to the disk for long term storage.
                    </li>
                    <li>
                        The definitions are written to the available memory upon starting the ClamAV-Daemon, and remain persistent through consistent checking of data
                        integrity.
                        <ul>
                            <li>
                                This is what effectively makes virus scanning so quick compared to a traditional system where you do not have to read the database from
                                disk constantly.
                            </li>
                        </ul>
                    </li>
                    <li>Doing the math, 0.5 GB/s / 10 GB/s = ~50 Milliseconds to iterate through the whole list. (excluding CPU time)</li>
                </ul>
            </li>
        </ul>
        Using the information provided above, this allows us to scan any file virtually under 100 milliseconds flat.
    </div>

    <div class="row border-bottom white-bg dashboard-header">
        <section id="section-routes"></section>
        <h2>Routes</h2>
        Routes are essentially what is known as the URLs that you can call to interact with pitter.
    </div>
@endsection