@extends('layouts.panel')
@section("title", "Dashboard")
@section("body")
    <br>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-sm-3">
                        <h1 class="m-b-xs">
                            {{ $timed["views"] }}
                        </h1>
                        <small>
                            Views this month
                        </small>
                        <div id="sparkline1" class="m-b-sm">
                            <canvas width="491" height="50" style="display: inline-block; width: 491.984px; height: 50px; vertical-align: top;"></canvas>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <small class="stats-label">Total amount of views for all uploads</small>
                                <h4>{{ $total["views"] }}</h4>
                            </div>

                        </div>

                    </div>
                    <div class="col-sm-3">
                        <h1 class="m-b-xs">
                            {{ $timed["files"] }}
                        </h1>
                        <small>
                            Uploads this month
                        </small>
                        <div id="sparkline2" class="m-b-sm">
                            <canvas width="491" height="50" style="display: inline-block; width: 491.984px; height: 50px; vertical-align: top;"></canvas>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <small class="stats-label">Total number of uploads</small>
                                <h4>{{ $total["files"] }}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <h1 class="m-b-xs">
                            {{ $timed["bandwidth"]["formatted"] }}
                        </h1>
                        <small>
                            Bandwidth consumed this month
                        </small>
                        <div id="sparkline3" class="m-b-sm">
                            <canvas width="491" height="50" style="display: inline-block; width: 491.984px; height: 50px; vertical-align: top;"></canvas>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <small class="stats-label">Total amount of bandwidth consumed for all uploads</small>
                                <h4>{{ $total["bandwidth"]["formatted"] }}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <h1 class="m-b-xs">
                            {{ $timed["space"]["formatted"] }}
                        </h1>
                        <small>
                            Space consumed this month
                        </small>
                        <div id="sparkline4" class="m-b-sm">
                            <canvas width="491" height="50" style="display: inline-block; width: 491.984px; height: 50px; vertical-align: top;"></canvas>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <small class="stats-label">Total amount of disk space consumed for all uploads</small>
                                <h4>{{ $total["space"]["formatted"] }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="small pull-left col-md-3 m-l-lg m-t-md">
                        <strong>Monthly Activity Graph</strong>
                    </div>
                    <div class="small pull-right col-md-3 m-t-md text-right">
                        Still working on this
                    </div>
                    <div class="flot-chart m-b-xl">
                        <div class="flot-chart-content" id="flot-dashboard5-chart" style="padding: 0px; position: relative;">
                            <canvas class="flot-base" width="1536" height="200"
                                    style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1536px; height: 200px;"></canvas>
                            <div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                                <div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;">
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 17px; text-align: center;">0
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 111px; text-align: center;">1
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 205px; text-align: center;">2
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 300px; text-align: center;">3
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 394px; text-align: center;">4
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 488px; text-align: center;">5
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 583px; text-align: center;">6
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 677px; text-align: center;">7
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 771px; text-align: center;">8
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 866px; text-align: center;">9
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 957px; text-align: center;">10
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 1051px; text-align: center;">11
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 1145px; text-align: center;">12
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 1240px; text-align: center;">13
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 1334px; text-align: center;">14
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 1428px; text-align: center;">15
                                    </div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; max-width: 90px; top: 183px; left: 1523px; text-align: center;">16
                                    </div>
                                </div>
                                <div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;">
                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 171px; left: 8px; text-align: right;">0</div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 137px; left: 8px; text-align: right;">5</div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 103px; left: 2px; text-align: right;">10</div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 69px; left: 2px; text-align: right;">15</div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 35px; left: 2px; text-align: right;">20</div>
                                    <div class="flot-tick-label tickLabel" style="position: absolute; top: 1px; left: 2px; text-align: right;">25</div>
                                </div>
                            </div>
                            <canvas class="flot-overlay" width="1536" height="200"
                                    style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1536px; height: 200px;"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">


                <div class="ibox-content">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="product_name">Project Name</label>
                                <input type="text" id="product_name" name="product_name" value="" placeholder="Project Name" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="price">Name</label>
                                <input type="text" id="price" name="price" value="" placeholder="Name" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label" for="quantity">Company</label>
                                <input type="text" id="quantity" name="quantity" value="" placeholder="Company" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="status">Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="1" selected="">Completed</option>
                                    <option value="0">Pending</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped">

                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Master project</td>
                                <td>Patrick Smith</td>
                                <td>$892,074</td>
                                <td>Inceptos Hymenaeos Ltd</td>
                                <td><strong>20%</strong></td>
                                <td>Jul 14, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Alpha project</td>
                                <td>Alice Jackson</td>
                                <td>$963,486</td>
                                <td>Nec Euismod In Company</td>
                                <td><strong>40%</strong></td>
                                <td>Jul 16, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Betha project</td>
                                <td>John Smith</td>
                                <td>$996,824</td>
                                <td>Erat Volutpat</td>
                                <td><strong>75%</strong></td>
                                <td>Jul 18, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Gamma project</td>
                                <td>Anna Jordan</td>
                                <td>$105,192</td>
                                <td>Tellus Ltd</td>
                                <td><strong>18%</strong></td>
                                <td>Jul 22, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Alpha project</td>
                                <td>Alice Jackson</td>
                                <td>$674,803</td>
                                <td>Nec Euismod In Company</td>
                                <td><strong>40%</strong></td>
                                <td>Jul 16, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Master project</td>
                                <td>Patrick Smith</td>
                                <td>$174,729</td>
                                <td>Inceptos Hymenaeos Ltd</td>
                                <td><strong>20%</strong></td>
                                <td>Jul 14, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Gamma project</td>
                                <td>Anna Jordan</td>
                                <td>$823,198</td>
                                <td>Tellus Ltd</td>
                                <td><strong>18%</strong></td>
                                <td>Jul 22, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Project
                                    <small>This is example of project</small>
                                </td>
                                <td>Patrick Smith</td>
                                <td>$778,696</td>
                                <td>Inceptos Hymenaeos Ltd</td>
                                <td><strong>20%</strong></td>
                                <td>Jul 14, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Alpha project</td>
                                <td>Alice Jackson</td>
                                <td>$861,063</td>
                                <td>Nec Euismod In Company</td>
                                <td><strong>40%</strong></td>
                                <td>Jul 16, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Betha project</td>
                                <td>John Smith</td>
                                <td>$109,125</td>
                                <td>Erat Volutpat</td>
                                <td><strong>75%</strong></td>
                                <td>Jul 18, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Gamma project</td>
                                <td>Anna Jordan</td>
                                <td>$600,978</td>
                                <td>Tellus Ltd</td>
                                <td><strong>18%</strong></td>
                                <td>Jul 22, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Alpha project</td>
                                <td>Alice Jackson</td>
                                <td>$150,161</td>
                                <td>Nec Euismod In Company</td>
                                <td><strong>40%</strong></td>
                                <td>Jul 16, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Project
                                    <small>This is example of project</small>
                                </td>
                                <td>Patrick Smith</td>
                                <td>$160,586</td>
                                <td>Inceptos Hymenaeos Ltd</td>
                                <td><strong>20%</strong></td>
                                <td>Jul 14, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Gamma project</td>
                                <td>Anna Jordan</td>
                                <td>$110,612</td>
                                <td>Tellus Ltd</td>
                                <td><strong>18%</strong></td>
                                <td>Jul 22, 2015</td>
                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section("body2")
    <div class="wrapper wrapper-content animated fadeIn">

        <div class="p-w-md m-t-sm">
            <div class="row text-center">
                <div class="col-sm-3">
                    <h1 class="m-b-xs">
                        {{ $total["files"] }}
                    </h1>
                    <small>
                        Uploaded Files
                    </small>
                </div>
                <div class="col-sm-3">
                    <h1 class="m-b-xs">
                        {{ $total["views"] }}
                    </h1>
                    <small>
                        Views
                    </small>
                </div>
                <div class="col-sm-3">
                    <h1 class="m-b-xs">
                        {{ $total["space"]["formatted"] }}
                    </h1>
                    <small>
                        Space Consumed
                    </small>
                </div>
                <div class="col-sm-3">
                    <h1 class="m-b-xs">
                        {{ $total["bandwidth"]["formatted"] }}
                    </h1>
                    <small>
                        Bandwidth Consumed
                    </small>
                </div>
            </div>
        </div>
        <hr>
        <h3>
        </h3>
        <div class="row">
            <div class="col-lg-12">
                <div class="small pull-left col-md-3 m-l-lg m-t-md">
                    <strong>
                        <span style="color: #1ab394; ">
                            Views
                        </span>
                        <br>
                        <span style="color: #1C84C6; ">
                            Uploads
                        </span>
                    </strong>
                </div>
                <div class="small pull-right col-md-3 m-t-md text-right">
                    <strong>
                        This graph is an interactive visualization of the last month
                        <br>
                        Data is measured in UTC/GMT +0
                        <br>
                        Updating every 5 seconds
                    </strong>
                </div>
                <div class="flot-chart m-b-xl">
                    <div class="flot-chart-content" id="flot-dashboard5-chart" style=""></div>
                </div>
            </div>
        </div>
        <hr>

    </div>
@endsection
@section("scripts")
    <script>
        $(document).ready(function () {

            var sparklineCharts = function () {
                $("#sparkline1").sparkline([34, 43, 43, 35, 44, 32, 44, 52], {
                    type: 'line',
                    width: '100%',
                    height: '50',
                    lineColor: '#1ab394',
                    fillColor: "transparent"
                });

                $("#sparkline2").sparkline([32, 11, 25, 37, 41, 32, 34, 42], {
                    type: 'line',
                    width: '100%',
                    height: '50',
                    lineColor: '#1ab394',
                    fillColor: "transparent"
                });

                $("#sparkline3").sparkline([34, 22, 24, 41, 10, 18, 16, 8], {
                    type: 'line',
                    width: '100%',
                    height: '50',
                    lineColor: '#1C84C6',
                    fillColor: "transparent"
                });
            };

            var sparkResize;

            $(window).resize(function (e) {
                clearTimeout(sparkResize);
                sparkResize = setTimeout(sparklineCharts, 500);
            });

            sparklineCharts();


            var data1 = [
                [0, 4], [1, 8], [2, 5], [3, 10], [4, 4], [5, 16], [6, 5], [7, 11], [8, 6], [9, 11], [10, 20], [11, 10], [12, 13], [13, 4], [14, 7], [15, 8], [16, 12]
            ];
            var data2 = [
                [0, 0], [1, 2], [2, 7], [3, 4], [4, 11], [5, 4], [6, 2], [7, 5], [8, 11], [9, 5], [10, 4], [11, 1], [12, 5], [13, 2], [14, 5], [15, 2], [16, 0]
            ];
            $("#flot-dashboard5-chart").length && $.plot($("#flot-dashboard5-chart"), [
                    data1, data2
                ],
                {
                    series: {
                        lines: {
                            show: false,
                            fill: true
                        },
                        splines: {
                            show: true,
                            tension: 0.4,
                            lineWidth: 1,
                            fill: 0.4
                        },
                        points: {
                            radius: 0,
                            show: true
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,

                        borderWidth: 2,
                        color: 'transparent'
                    },
                    colors: ["#1ab394", "#1C84C6"],
                    xaxis: {},
                    yaxis: {},
                    tooltip: false
                }
            );

        });
    </script>



@endsection