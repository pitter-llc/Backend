<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Pitter | @yield("title")</title>

    <link href="/dashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="/dashboard/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/dashboard/css/animate.css" rel="stylesheet">
    <link href="/dashboard/css/style.css" rel="stylesheet">
    <link href="/dashboard/css/plugins/toastr/toastr.min.css" rel="stylesheet">

</head>

<body class="md-skin">
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <span>
                            <img alt="image" class="img-circle" height="50"
                                 src="https://gravatar.com/avatar/{{ md5(strtolower(trim(\Illuminate\Support\Facades\Auth::user()->email))) }}">
                        </span>
                        <a href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold">
                                        {{ \Illuminate\Support\Facades\Crypt::decrypt(\Illuminate\Support\Facades\Auth::user()->name) }}
                                    </strong>
                                </span>
                                <span class="text-muted text-xs block">
                                    {{ \App\User::getRankTitle() }} Account
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="logo-element">
                        Pitter
                    </div>
                </li>
                <!-- MAIN NAV -->
                <li class="">
                    <a href="/">
                        <i class="fa fa-th-large"></i>
                        <span class="nav-label">Dashboard</span>
                    </a>
                </li>
                <li class="">
                    <a href="/uploads">
                        <i class="fa fa-upload"></i>
                        <span class="nav-label">Uploads</span>
                    </a>
                </li>
                <li class="">
                    <a href="#">
                        <i class="fa fa-cogs"></i>
                        <span class="nav-label">Settings</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="/settings/account">Account</a></li>
                    </ul>
                </li>
                <li class="">
                    <a href="#">
                        <i class="fa fa-code"></i>
                        <span class="nav-label">Developer Center</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="/developer/profile">Developer Profile</a></li>
                        <li><a href="https://github.com/Elycin/pitter-documentation/wiki" target="_blank">Documentation</a></li>
                        <li><a href="/developer/application-keys">API Keys</a></li>
                        <li><a href="/developer/applications">Applications</a></li>
                    </ul>
                </li>
                <li class="">
                    <a href="/workshop">
                        <i class="fa fa-gavel"></i>
                        <span class="nav-label">Workshop</span>
                    </a>
                </li>
                <li class="">
                    <a href="#">
                        <i class="fa fa-question-circle"></i>
                        <span class="nav-label">Help</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="/help/terms-of-service">Terms of Service</a></li>
                        <li><a href="/help/data-policy">Data Policy</a></li>
                        <li><a href="/help/tutorial">How to use</a></li>
                        <li><a href="/help/change-logs">Changelogs</a></li>
                    </ul>
                </li>
                <!-- MAIN NAV END -->
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                        <!-- collapse -->
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="/logout">
                            <i class="fa fa-sign-out"></i>
                            Log out
                        </a>
                    </li>
                    <li>
                        <a class="right-sidebar-toggle">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- MAIN ELEMENT -->
        @yield("body")
    </div>
    <div id="right-sidebar" class="animated">
        <div class="sidebar-container">
            <ul class="nav nav-tabs navs-3">
                <li class="active">
                    <a data-toggle="tab" href="#tab-1">
                        Activity
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-2">
                        Upload
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#tab-3">
                        Toolbox
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="sidebar-title">
                        <h3><i class="fa fa-list"></i> Recent Activity</h3>
                    </div>
                    <ul class="sidebar-list">
                        @foreach(\App\Upload::where('user_id', \Illuminate\Support\Facades\Auth::user()->id)->orderBy('id', 'desc')->take(10)->get() as $upload)
                            <li>
                                <a href="https://i.pitter.us/{{ $upload->filename }}" target="_blank">
                                    <div class="small pull-right m-t-xs">{{ \Carbon\Carbon::parse($upload->created_at)->toDateTimeString() }}</div>
                                    <h4>File Uploaded</h4>
                                    You Uploaded {{ $upload->filename }}<br>
                                    ({{ $upload->original_filename }})
                                </a>
                            </li>
                        @endforeach

                    </ul>
                </div>

                <div id="tab-2" class="tab-pane upload-pane drag-and-drop">
                    <div class="sidebar-title">
                        <h3><i class="fa fa-upload"></i> Drag and Drop File Upload</h3>
                        <hr>
                        <small>
                            Drop anywhere on this pane to upload.
                            <br>
                            Files may not exceed 100 MB per.
                        </small>
                    </div>
                    <ul class="sidebar-list" id="drag-and-drop-list">

                    </ul>
                </div>

                <div id="tab-3" class="tab-pane ">
                    <div class="sidebar-title">
                        <h3><i class="fa fa-cogs"></i> Toolbox</h3>

                        @if(!strpos(\Illuminate\Support\Facades\Request::url(), '/uploads') !== false)
                            <hr>
                            <small>
                                Toolbox is only available for uploads.
                            </small>
                        @endif

                    </div>
                    <ul class="sidebar-list">
                        @if(strpos(\Illuminate\Support\Facades\Request::url(), '/uploads') !== false)
                            <li>
                                <h4>Delete Mode</h4>
                                <small>
                                    With this mode enabled, any table row or image you select will be deleted from your account permanently. Do not underestimate!
                                </small>
                                <br><br>
                                <div class="onoffswitch">
                                    <input type="checkbox" class="onoffswitch-checkbox" id="toggle-delete-mode">
                                    <label class="onoffswitch-label" for="toggle-delete-mode">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Mainly scripts -->
<script src="/dashboard/js/jquery-3.1.1.min.js"></script>
<script src="/dashboard/js/bootstrap.min.js"></script>
<script src="/dashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/dashboard/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/dashboard/js/plugins/toastr/toastr.min.js"></script>

<!-- Flot -->
<script src="/dashboard/js/plugins/flot/jquery.flot.js"></script>
<script src="/dashboard/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="/dashboard/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="/dashboard/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="/dashboard/js/plugins/flot/jquery.flot.pie.js"></script>
<script src="/dashboard/js/plugins/flot/jquery.flot.time.js"></script>

<!-- Sparkline -->
<script src="/dashboard/js/plugins/sparkline/jquery.sparkline.min.js"></script>


<!-- Peity -->
<script src="/dashboard/js/plugins/peity/jquery.peity.min.js"></script>
<script src="/dashboard/js/demo/peity-demo.js"></script>

<!-- Custom and plugin javascript -->
<script src="/dashboard/js/inspinia.js"></script>
<script src="/dashboard/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="/dashboard/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<script src="/dashboard/js/pitter.js"></script>

@yield("scripts")

<script>
    //Before document complete
    var sidebar_obj = $("#side-menu").children();
    $.each(sidebar_obj, function (li_index, li_value) {
        var sidebar_obj_index_a = $(li_value).find("a");
        if (sidebar_obj_index_a.attr("href") == window.location.pathname) {
            $(li_value).addClass("active");
        }
    });
</script>

</body>
</html>