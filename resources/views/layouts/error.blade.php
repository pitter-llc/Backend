<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <title>Pitter | @yield("code")</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="http://pitter.us/assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet"/>
    <link href="http://pitter.us/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="http://pitter.us/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="http://pitter.us/assets/css/animate.min.css" rel="stylesheet"/>
    <link href="http://pitter.us/assets/css/style.min.css" rel="stylesheet"/>
    <link href="http://pitter.us/assets/css/style-responsive.min.css" rel="stylesheet"/>
    <link href="http://pitter.us/assets/css/theme/default.css" rel="stylesheet" id="theme"/>
    <!-- ================== END BASE CSS STYLE ================== -->

</head>
<body class="pace-top">
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade">
    <!-- begin error -->
    <div class="error">
        <div class="error-code m-b-10">@yield("code") <i class="fa @yield("fa")"></i></div>
        <div class="error-content">
            <div class="error-message">@yield("h1")</div>
            <div class="error-desc m-b-20">
                @yield("message")
                @if(strpos($_SERVER["HTTP_HOST"], 'pitter.us') === false)
                    <br><br><br><br><br><br>
                    <div class="alert alert-info col-md-4 col-md-offset-4">
                        <strong>Disclaimer</strong><br>
                        The domain you have accessed this page from is routing a subdomain to our servers.<br>
                        We cannot guarantee any accuracy of content uploaded by the user of this domain.
                    </div>
                @endif
            </div>
        </div>
    </div>
    <!-- end error -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="http://pitter.us/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="http://pitter.us/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="http://pitter.us/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="http://pitter.us/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
<script src="http://pitter.us/assets/crossbrowserjs/html5shiv.js"></script>
<script src="http://pitter.us/assets/crossbrowserjs/respond.min.js"></script>
<script src="http://pitter.us/assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="http://pitter.us/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="http://pitter.us/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="http://pitter.us/assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function () {
        App.init();
    });
</script>
</body>
</html>
