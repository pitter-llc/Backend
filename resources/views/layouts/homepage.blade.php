<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <title>Pitter | Simple screenshot and file uploading</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <meta content="Simple screenshot and file uploading" name="description"/>
    <meta content="Pitter" name="author"/>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"/>

    <link href="/css/style.css" rel="stylesheet"/>
    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="/css/animate.css" rel="stylesheet"/>
    <link href="/css/mdb.min.css" rel="stylesheet">

    <!-- Template styles -->
    <style>
        /* TEMPLATE STYLES */

        html,
        body,
        .view-main {
            height: 100%;
        }

        /* Navigation*/

        .navbar {
            background-color: transparent;
        }

        .scrolling-navbar {
            -webkit-transition: background .5s ease-in-out, padding .5s ease-in-out;
            -moz-transition: background .5s ease-in-out, padding .5s ease-in-out;
            transition: background .5s ease-in-out, padding .5s ease-in-out;
        }

        .top-nav-collapse {
            background-color: #1C2331;
        }

        footer.page-footer {
            background-color: #1C2331;
            margin-top: 2rem;
        }

        @media only screen and (max-width: 768px) {
            .navbar {
                background-color: #1C2331;
            }
        }

        /*Call to action*/

        .flex-center {
            color: #fff;
        }

        .view-main {
            background: url("/img/backgrounds/home.jpg") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .view-milestone {
            background: url("/img/backgrounds/milestone.jpg") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .view-overlay {
            background-color: rgba(0, 0, 0, .7);
        }

        .foot-text {
            color: #8F8E8E;
        }

        /*Contact section*/

        #contact .fa {
            font-size: 2.5rem;
            margin-bottom: 1rem;
            color: #1C2331;
        }
    </style>
</head>
<body data-spy="scroll" data-target="#header-navbar" data-offset="51">
<!-- begin #page-container -->
@yield('content')
<!-- begin #footer -->
<div id="footer" class="footer">
    <div class="container">
        <div class="footer-brand">
            <div class=""><img src="/old/homepage/assets/img/256x256.png" width="80" style="margin-top: -4px;"/>
            </div>
            Pitter
        </div>
        <p class="foot-text">
            Made in Torrance, CA and Port Saint Lucie, FL
            <br>
            &copy; Pitter, LLC 2016-2017
            <br><br>
        </p>
        <p>
            <a href="/">HOME</a> |
            <a href="/abuse">ABUSE</a> |
            <a href="/contact">CONTACT</a> |
            <a href="/terms">TERMS OF SERVICE</a> |
            <a href="/data">DATA POLICY</a> |
            <a href="/cookies">COOKIE POLICY</a>
        </p>
    </div>
</div>
</body>
<footer>
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/tether.min.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>

    <script src="/old/homepage/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <!--[if lt IE 9]>
    <script src="/old/homepage/assets/crossbrowserjs/html5shiv.js"></script>
    <script src="/old/homepage/assets/crossbrowserjs/respond.min.js"></script>
    <script src="/old/homepage/assets/crossbrowserjs/excanvas.min.js"></script>
    <![endif]-->
    <script src="/old/homepage/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
    <script src="/old/homepage/assets/plugins/scrollMonitor/scrollMonitor.js"></script>
    <script src="/old/homepage/assets/js/apps.min.js"></script>

    @yield('js')

    <script>
        $(document).ready(function () {
            App.init();
            new WOW().init();
        });
    </script>
</footer>



