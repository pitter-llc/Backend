@extends("layouts.error")
@section("code", "404")
@section("fa", "fa-exclamation-triangle")
@section("h1", "File not found")
@section("message",
"We're unable to find the file you have requested.
 <br>
 If you are sure that it exists, there is a possibility of a server outage somewhere.
 <br><br>
 <a href='https://panel.pitter.us/' class='btn btn-success'>Go to the panel</a>")