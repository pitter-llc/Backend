<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->integer('user_id');
            $table->string('filename')->unique()->index();
            $table->string('original_filename');
            $table->bigInteger('size');
            $table->bigInteger('encrypted_size');
            $table->string('mime');
            $table->boolean("encrypted")->default(true);
            $table->bigInteger('views')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('uploads');
    }
}
