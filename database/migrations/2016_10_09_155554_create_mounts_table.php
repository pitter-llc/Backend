<?php

use Illuminate\Database\Migrations\Migration;

class CreateMountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mounts', function ($table) {
            $table->increments('id')->unique()->index();
            $table->string('hostname')->unique();
            $table->string('wan_ip')->unique();
            $table->string('vlan_ip')->unique();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->bigInteger('disk_available')->default(0);
            $table->bigInteger('disk_total')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mounts');
    }
}
