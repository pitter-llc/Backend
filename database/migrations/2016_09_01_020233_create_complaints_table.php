<?php

use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function ($table) {
            $table->increments('id')->unique();
            $table->string('reference');
            $table->integer('name');
            $table->integer('email');
            $table->string('file');
            $table->integer('type');
            $table->string('body');
            $table->string('response');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('complaints');
    }
}
