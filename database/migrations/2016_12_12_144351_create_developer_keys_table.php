<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeveloperKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('developer_keys', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('user_id');
            $table->string('application')->default("Unassigned");
            $table->string('key');
            $table->string('secret');
            $table->boolean('joint')->default(false);
            $table->bigInteger('usage_counter')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('developer_keys');
    }
}
