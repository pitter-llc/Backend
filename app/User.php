<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'rank',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public static function isSuspended()
    {
        return (Auth::user()->rank == -1);
    }

    public static function isUnverified()
    {
        return (Auth::user()->rank == 0);
    }

    public static function isMember()
    {
        return (Auth::user()->rank == 1);
    }

    public static function isPremium()
    {
        return (Auth::user()->rank >= 2);
    }

    public static function isAdmin()
    {
        return (Auth::user()->rank >= 9);
    }

    public static function getEmail()
    {
        return Auth::user()->email;
    }

    public static function getName()
    {
        return Crypt::decrypt(Auth::user()->name);
    }

    public static function getRank()
    {
        return Auth::user()->rank;
    }

    public static function getRankTitle()
    {
        switch (Auth::user()->rank) {
            case -1: {
                return "Suspended";
            }
            case 0: {
                return "Unverified";
            }
            case 1: {
                return "Standard";
            }
            case 2: {
                return "Premium";
            }
            case 3: {
                return "Beta Tester";
            }
            case 9: {
                return "Administrator";
            }
            case 10: {
                return "Owner";
            }
        }
    }
}
