<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeveloperKey extends Model
{
    //
    protected $table = "developer_keys";

    public static function isValidKey($key, $secret){
        if (DeveloperKey::where([
            "key" => $key,
            "secret" => $secret
        ])->first()){
            return true;
        } else {
            return false;
        }
    }

    public static function isClientKey($key, $secret){
        if (DeveloperKey::where([
            "key" => $key,
            "secret" => $secret,
            "will_others_use" => true
        ])->first()){
            return true;
        } else {
            return false;
        }
    }
}
