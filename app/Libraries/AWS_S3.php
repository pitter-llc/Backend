<?php
/**
 * Created by PhpStorm.
 * User: elycin
 * Date: 3/11/17
 * Time: 3:39 PM
 */

namespace App\Libraries;


use Illuminate\Support\Facades\Storage;

class AWS_S3
{
    private $s3;

    public function __construct()
    {
        //Initialize Amazon S3 Connection
        try {
            $this->s3 = Storage::disk('s3');
        } catch (\Exception $exception) {
            return abort(500);
        }
    }

    public function exists($filename)
    {
        return $this->s3->exists($filename);
    }

    public function has($filename)
    {
        return $this->s3->exists($filename);
    }

    public function get($filename)
    {
        return $this->s3->get($filename);
    }

    public function upload($filename, $data)
    {
        return $this->s3->put($filename, $data);
    }

    public function delete($filename)
    {
        return $this->s3->delete($filename);
    }

    public function getAmazonS3Connection()
    {
        return $this->s3;
    }
}