<?php
/**
 * Created by PhpStorm.
 * User: elycin
 * Date: 10/24/16
 * Time: 1:59 AM
 */

namespace App\Libraries;


use App\Upload;
use Illuminate\Support\Facades\Auth;

class Pitter
{
    public static $DEFINITIONS = [
        "SQL" => [
            "extensions" => ["sql", "db"],
            "mimes" => []
        ],
        "EEPROMs" => [
            "extensions" => ["epr", "eeprom"],
            "mimes" => []
        ],
        "RSA/PGP" => [
            "extensions" => ["pub"],
            "mimes" => ["application/pgp"]
        ],
        "Scripts" => [
            "extensions" => ["c", "cpp", "php", "java", "h", "py"],
            "mimes" => ["text/pascal", "text/x-h", "text/x-c", "text/x-java-source"]
        ],
        "Compiled Bytecode" => [
            "extensions" => ["class", "pyc"],
            "mimes" => ["application/java"]
        ],
        "Images" => [
            "extensions" => [],
            "mimes" => ["image/png", "image/jpg", "image/jpeg"]
        ],
        "GIFs" => [
            "extensions" => ["gif", "gifv"],
            "mimes" => ["image/gif"]
        ],
        "Video" => [
            "extensions" => ["mp4", "avi", "mov", "mkv", "flv"],
            "mimes" => ["video/avi", "video/mpeg"]
        ],
        "Audio" => [
            "extensions" => ["mp3", "midi", "flac", "aac", "wav", "aiff"],
            "mimes" => ["audio/mpeg"]
        ],
        "HTML" => [
            "extensions" => ["html", "shtml"],
            "mimes" => ["text/html"]
        ],
        "Archives" => [
            "extensions" => ["gz", "tar", "tar.gz", "rar", "zip", "bzip"],
            "mimes" => ["application/zip", "application/x-compressed", "application/x-rar-compressed", "application/x-gzip"]
        ],
        "PDFs" => [
            "extensions" => ["pdf"],
            "mimes" => ["application/pdf"]
        ],
        "Documents" => [
            "extensions" => ["doc", "docx", "rtf", "txt"],
            "mimes" => ["application/msword", "text/plain"]
        ],
        "Applications" => [
            "extensions" => ["exe"],
            "mimes" => ["application/octet-stream"]
        ],
    ];
    private $utilities;
    private $last_lookup;

    public function __construct()
    {
        $this->utilities = new Utilities();
    }

    public function getFilesFancy()
    {
        $files = Upload::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
        $categories = [];
        foreach ($files as $file) {
            $loc_info = new \SplFileInfo($file->original_filename);
            $ext = strtolower(trim($loc_info->getExtension()));
            if ($this->lookupFiletype($ext, $file->mime)) {
                if (!array_key_exists($this->last_lookup, $categories)) {
                    $categories[$this->last_lookup] = []; //Make it an array.
                }
                $categories[$this->last_lookup][] = $file;
            } else {
                //Other
                if (!array_key_exists("Unknown", $categories)) {
                    $categories["Unknown"] = []; //Make it an array.
                }
                $categories["Unknown"][] = $file;
            }
        }

        ksort($categories);
        return $categories;
    }

    private function lookupFiletype($extension, $mime)
    {
        foreach (self::$DEFINITIONS as $category => $category_values) {
            if (in_array($extension, $category_values["extensions"]) || in_array($mime, $category_values["mimes"])) {
                $this->last_lookup = $category;
                return true;
            }
        }
        return false;
    }
}