<?php

namespace App\Libraries;

class ClamAV
{
    private $socket;
    private $socket_buffer = 1024;

    public function __construct()
    {
        if (file_exists(env('CLAM_SOCK', '/var/run/clamav/clamd.ctl'))) $this->socket = fsockopen("unix://" . env('CLAM_SOCK', '/var/run/clamav/clamd.ctl'));
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        fclose($this->socket);
    }

    public function connectSocketOverTCP($address, $port)
    {
        $this->socket = fsockopen($address, $port);
    }

    public function connectSocketOverUnix($path)
    {
        $this->socket = fsockopen(sprintf("unix://%s", $path));
    }

    public function ping()
    {
        return $this->sendCommand("PING");
    }

    private function sendCommand($command)
    {
        fwrite($this->socket, "n$command\n");
        return trim(fread($this->socket, $this->socket_buffer));
    }

    public function getVersion()
    {
        return $this->sendCommand("VERSION");
    }

    public function getStatus()
    {
        return $this->sendCommand("STATS");
    }

    public function reload()
    {
        return $this->sendCommand("RELOAD");
    }

    public function isFileVirus($filename)
    {
        return strpos(explode(": ", $this->sendCommand("SCAN $filename"))[1], 'FOUND') !== false;
    }
}