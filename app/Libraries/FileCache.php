<?php
/**
 * Created by PhpStorm.
 * User: elycin
 * Date: 3/11/17
 * Time: 3:41 PM
 */

namespace App\Libraries;


use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FileCache
{
    private $directory_prefix = 'cache';
    private $cache_directory;
    private $reserved_disk_space = 0;
    private $reserved_disk_space_percent_in_decimal_form = 0.05;
    private $cache_time = 1 * (((60 ^ 2) * 24) * 14);

    public function __construct()
    {
        $this->cache_directory = storage_path($this->directory_prefix);
        if (!is_dir($this->cache_directory)) mkdir($this->cache_directory);
        $this->reserved_disk_space = disk_total_space($this->cache_directory) * $this->reserved_disk_space_percent_in_decimal_form;
    }

    public function has($filename)
    {
        return Storage::exists(sprintf("%s/%s", $this->directory_prefix, $filename));
    }

    public function get($filename)
    {
        return Storage::get(sprintf("%s/%s", $this->directory_prefix, $filename));
    }

    public function put($filename, $data)
    {
        return Storage::put(sprintf("%s/%s", $this->directory_prefix, $filename), $data);
    }

    public function isEnoughSpace($bytes)
    {
        return disk_free_space(storage_path($this->directory_prefix)) - $bytes > $this->reserved_disk_space;
    }

    public function getEarliestCachedFilename()
    {
        $oldest = [
            "filename" => "",
            "timestamp" => Carbon::now()->timestamp
        ];

        foreach ($this->getFileList() as $file) {
            if (File::lastModified($file) > $oldest["timestamp"]) {
                $oldest["filename"] = $file;
            }
        }

        return $oldest["filename"];
    }

    public function getFileList()
    {
        return Storage::files($this->directory_prefix);
    }

    public function cleanup()
    {
        foreach ($this->getFileList() as $file) {
            if (File::lastModified($file) > $this->cache_time) {
                $this->delete($file);
            }
        }
    }

    public function delete($filename)
    {
        return Storage::delete(sprintf("%s/%s", $this->directory_prefix, $filename));
    }
}