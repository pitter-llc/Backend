<?php
/**
 * Created by PhpStorm.
 * User: elycin
 * Date: 3/12/17
 * Time: 5:50 PM
 */

namespace App\Libraries;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManagerStatic;

class ThumbnailHandler
{

    private $thumbnail_prefix = "thumbnails";
    private $generated_filepath;
    private $file_metadata;
    private $image_height;
    private $image_width;

    public function __construct(Collection $file_metadata, $height = 100, $width = 100)
    {
        $this->file_metadata = $file_metadata;
        $this->generated_filepath = sprintf("%s/%s", $this->thumbnail_prefix, $this->file_metadata->filename);
        $this->image_height = $height;
        $this->image_width = $width;
    }

    public function exists()
    {
        return Storage::exists($this->generated_filepath);
    }

    public function deleteThumbnail()
    {
        return Storage::delete($this->generated_filepath);
    }

    public function createFromCacheInstance(FileCache $fileCache)
    {
        try {
            ImageManagerStatic::make($fileCache->get($this->file_metadata->filename))->resize(100, 100)->save(storage_path($this->generated_filepath));
        } catch (\Exception $exception) {
            return false;
        }
        return $this->getThumbnail();
    }

    public function getThumbnail()
    {
        return Image::make($this->generated_filepath)->response();
    }

}