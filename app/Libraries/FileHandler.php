<?php
/**
 * Created by PhpStorm.
 * User: elycin
 * Date: 3/11/17
 * Time: 5:17 PM
 */

namespace App\Libraries;

use App\Upload;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class FileHandler
{
    public static $RESPONSES = [
        "StorageErrorAWSRequest" => [
            "title" => "External Storage Failure",
            "body" => "An error occurred while attempting to process your request on the external storage array",
            "type" => "danger"
        ],
        "StorageErrorCacheRequest" => [
            "title" => "Storage Array Failure",
            "body" => "An error occurred while attempting to process your request in the cache",
            "type" => "danger"
        ],
        "StorageErrorAWSDeletion" => [
            "title" => "Failed to delete file",
            "body" => "An error occurred with the external storage array while attempting to delete your file",
            "type" => "danger"
        ],
        "StorageErrorFileCacheDeletion" => [
            "title" => "Failed to delete file",
            "body" => "An error occurred while deleting the file from the caches.",
            "type" => "danger"
        ],
        "MalwareDetected" => [
            "title" => "Malicious File Detected",
            "body" => "Our servers have detected that the file you attempted to upload is malicious. File has been discarded.",
            "type" => "danger"
        ],
        "InvalidCredentials" => [
            "title" => "Invalid Credentials",
            "body" => "The email or password provided by your client provided was invalid.",
            "type" => "danger"
        ],
        "CredentialsNotProvided" => [
            "title" => "Invalid Headers",
            "body" => "The client you are using did not provide a properly formatted email or password.",
            "type" => "danger"
        ],
        "NotAuthenticated" => [
            "title" => "Authentication Failure",
            "body" => "The session you were previously logged into has expired. Please log in again.",
            "type" => "danger"
        ],
        "FileNotPassedInPostRequest" => [
            "title" => "Missing File Parameter",
            "body" => "The POST request you have sent did not have the 'file' parameter set",
            "type" => "danger"
        ],
        "InvalidPostRequest" => [
            "title" => "Invalid POST Request",
            "body" => "One or more fields you have provided was not recognized by the server. Please check that your form is formatted properly.",
            "type" => "warning"
        ],
        "AccountSuspended" => [
            "title" => "Invalid Permissions",
            "body" => "Your account has been prohibited from uploading.",
            "type" => "warning"
        ],
        "AccountQuota" => [
            "title" => "Insignificant Disk Space",
            "body" => "Your account has reached it's quota.",
            "type" => "warning"
        ],
        "UploadSuccessful" => [
            "title" => "Upload Successful",
            "body" => "A link to the file has been added to your clipboard.",
            "url" => "",
            "filename" => "",
            "type" => "success"
        ],
        "DeletionSuccessful" => [
            "title" => "File has been deleted",
            "body" => "%s no longer exists and cannot be recovered.",
            "type" => "success"
        ],
    ];
    public $Cache;
    private $AWS;
    private $ResponseHelper;

    public function __construct()
    {
        $this->AWS = new AWS_S3();
        $this->FileCache = new FileCache();
        $this->ResponseHelper = new ResponseHelper();
    }

    public function authCheck(Request $request)
    {
        /*
         * Authentication Measurement
         * Make sure that the user is authenticated, as this technically modifies their account properties.
         * */
        if (!Auth::check()) {
            if ($request->has("email") && $request->has("password")) {
                if (!Auth::attempt([
                    "email" => $request->input("email"),
                    "password" => $request->input("password")
                ])
                ) {
                    return (self::$RESPONSES["InvalidCredentials"]);
                }
            } else {
                return (self::$RESPONSES["CredentialsNotProvided"]);
            }
        } else {
            return (self::$RESPONSES["NotAuthenticated"]);
        }
    }

    public function upload($extension, $data_stream)
    {
        $NEW_FILENAME = $this->filenameGenerator($extension);

        try {
            $this->AWS->upload($NEW_FILENAME, $data_stream);
        } catch (\Exception $exception) {
            return self::$RESPONSES["StorageErrorAWSRequest"];
        }

        try {
            $this->FileCache->put($NEW_FILENAME, $data_stream);
        } catch (\Exception $exception) {
            return self::$RESPONSES["StorageErrorCacheRequest"];
        }
        return $this->ResponseHelper->successfulUpload(self::$RESPONSES["UploadSuccessful"], $NEW_FILENAME);
    }

    public function filenameGenerator($extension, $minlen = 4, $maxlen = 12)
    {
        while (true) {
            $avail_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
            $avail_chars_array = str_split($avail_chars);
            $num_of_chars = count($avail_chars_array) - 1;
            $new_fn = "";
            for ($i = 0; $i <= rand($minlen, $maxlen); $i++) {
                $new_fn .= $avail_chars_array[rand(0, $num_of_chars)];
            }
            $new_filename = $new_fn . "." . $extension;
            if (Upload::where('filename', '=', $new_filename)->count() == 0) {
                return $new_filename;
            }
        }
    }

    public function download(Collection $collection)
    {
        if (!$this->FileCache->has($collection->filename)) {
            if (!$this->AWS->has($collection->filename)) {
                return abort(404);
            } else {
                $DATA_STREAM = "";
                try {
                    $this->FileCache->put($collection->filename, $this->AWS->get($collection->filename));
                } catch (\Exception $exception) {
                    abort(500);
                }
                if (!$this->FileCache->isEnoughSpace($DATA_STREAM)) {
                    while (!$this->FileCache->isEnoughSpace($DATA_STREAM)) {
                        try {
                            $this->FileCache->delete($this->FileCache->getEarliestCachedFilename());
                        } catch (\Exception $exception) {
                            abort(500);
                        }
                    }
                }
                try {
                    $this->FileCache->put($collection->filename, $this->AWS->get($collection->filename));
                } catch (\Exception $exception) {
                    abort(500);
                }
            }
        }

        if (!isset($DATA_STREAM)) {
            $DATA_STREAM = $this->FileCache->get($collection->filename);
        }

        try {
            if ($collection->encrypted) $DATA_STREAM = Crypt::decrypt($DATA_STREAM);
        } catch (\Exception $exception) {
            abort(500);
        }

        /*
         * HEADER BUILDER
         * Start building headers for the response.
         * */
        $HEADERS = [
            "Content-Length" => strlen($DATA_STREAM),
            "Content-Type" => $collection->mime,
        ];
        if (strpos($collection->original_filename, "temp.") === false) {
            $HEADERS["Content-Disposition"] = sprintf('inline; filename="%s"', $collection->original_filename);
        }

        /*
         * SEND THE RESPONSE
         * */
        return response($DATA_STREAM, 200, $HEADERS);
    }

    public function delete(Request $request)
    {
        /*
         * Check to make sure a file was passed
         * */
        if (!$request->has("filename")) {
            return (self::$RESPONSES["InvalidPostRequest"]);
        }

        /*
         * Do the actual task
         * */
        if ($this->AWS->has($request->input("filename"))) {
            try {
                if (!$this->AWS->delete($request->input("filename"))) {
                    return (self::$RESPONSES["StorageErrorAWSDeletion"]);
                }
            } catch (\Exception $exception) {
                return (self::$RESPONSES["StorageErrorAWSRequest"]);
            }
        }
        if ($this->Cache->has($request->input("filename"))) {
            try {
                if (!$this->Cache->delete($request->input("filename"))) {
                    return (self::$RESPONSES["StorageErrorFileCacheDeletion"]);
                }
            } catch (\Exception $exception) {
                return (self::$RESPONSES["StorageErrorCacheRequest"]);
            }
        }

        /*
         * Return the response
         * */
        return (
        $this->ResponseHelper->successfulDeletion(
            self::$RESPONSES["DeletionSuccessful"],
            $request->input("filename")
        )
        );
    }
}