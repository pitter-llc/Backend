<?php
/**
 * Created by PhpStorm.
 * User: elycin
 * Date: 10/24/16
 * Time: 2:25 AM
 */

namespace App\Libraries;


use App\Graph;
use App\Mount;
use App\Upload;
use App\User;
use Illuminate\Support\Facades\Auth;
use Monolog\Handler\Curl\Util;

class Statistics
{
    private $utilities;

    public function __construct()
    {
        $this->utilities = new Utilities();
    }

    public function homepage()
    {
        $total_overall = \DB::table('uploads')->select(
            \DB::raw(
                'sum(`views`) as `views`, 
                sum(`encrypted_size`) as `storage`,
                sum(`views` * `encrypted_size`) as `bandwidth`'
            )
        )->first();

        return array(
            "user_count" => User::all()->count(),
            "files_uploaded" => Upload::all()->count(),
            "requests" => intval($total_overall->views),
            "space_consumed" => $this->utilities->sizeConverter(floatval($total_overall->storage)),
            "bandwidth" => $this->utilities->sizeConverter(floatval($total_overall->bandwidth)),
            "server_count" => Mount::all()->count()
        );
    }

    public static function downloadSize()
    {
        $stat = new Utilities();
        if (\App::environment('production')) {
            try {
                $bytes = 0;
                $files = scandir(base_path("public/download/"));
                foreach ($files as $file) {
                    $bytes += strlen(file_get_contents(base_path("public/download/" . $file)));
                }
                return $stat->sizeConverter($bytes, "MB", 2) . " MB";
            } catch (\Exception $exception) {
                return "Size Calculation Unavailable";
            }
        } else {
            return "Size Calculation Unavailable";
        }
    }

    public function dashboard($time)
    {
        $user_id = Auth::user()->id;

        //Get Total
        $user_total_uploads = \DB::table('uploads')->select(
            \DB::raw(
                'count(`filename`) as `uploads`, 
                sum(`views`) as `views`, 
                (sum(`views` * `size`)) as `bandwidth`, 
                sum(`encrypted_size`) as `storage`'
            )
        )->where(
            'user_id', '=', $user_id
        )->first();

        $temp_timed_set = Graph::select(['uploads', 'views', 'storage', 'bandwidth'])
            ->where('user_id', $user_id)
            ->orderBy('id', 'desc')
            ->take($time)
            ->get();

        $user_timed_uploads = array(
            "uploads" => $temp_timed_set->sum("uploads"),
            "views" => $temp_timed_set->sum("views"),
            "storage" => $temp_timed_set->sum("storage"),
            "bandwidth" => $temp_timed_set->sum("bandwidth"),
        );

        //Decay into parent
        return array(
            "graph" => Graph::where('user_id', $user_id)->orderBy('id', 'desc')->take($time)->get(),
            "total" => array(
                "files" => number_format($user_total_uploads->uploads),
                "views" => number_format($user_total_uploads->views),
                "space" => array(
                    "raw" => $user_total_uploads->storage,
                    "formatted" => number_format($this->utilities->sizeConverter($user_total_uploads->storage), 3) . " GB"
                ),
                "bandwidth" => array(
                    "raw" => $user_total_uploads->bandwidth,
                    "formatted" => number_format($this->utilities->sizeConverter($user_total_uploads->bandwidth), 3) . " GB"
                )
            ),
            "timed" => array(
                "files" => number_format($user_timed_uploads["uploads"]),
                "views" => number_format($user_timed_uploads["views"]),
                "space" => array(
                    "raw" => $user_timed_uploads["storage"],
                    "formatted" => number_format($this->utilities->sizeConverter($user_timed_uploads["storage"]), 3) . " GB"
                ),
                "bandwidth" => array(
                    "raw" => $user_timed_uploads["bandwidth"],
                    "formatted" => number_format($this->utilities->sizeConverter($user_timed_uploads["bandwidth"]), 3) . " GB"
                )
            )
        );
    }

    public function userGraphTemplate($user_id)
    {
        /*
         * This function will create a entry for today's date in the graphing table.
         * It should be called any time there is an upload or view.
         * */
        $check = Graph::where('user_id', $user_id)->whereRaw('Date(created_at) = CURDATE()')->count();

        if ($check == 0) {
            $g = new Graph;
            $g->user_id = $user_id;
            $g->save();
        }
    }


    public function logUpload($size)
    {
        Graph::where(
            [
                'user_id' => Auth::user()->id
            ]
        )->
        whereRaw('Date(created_at) = CURDATE()')->
        update(
            [
                'uploads' => \DB::raw('uploads + 1'),
                'storage' => \DB::raw('storage + ' . $size)
            ]
        );
    }

    public function logView($file_info)
    {
        //Make sure the uploader's graph exists.
        $this->userGraphTemplate($file_info->user_id);

        //Increments
        Upload::where('filename', $file_info->filename)->update(['views' => \DB::raw('views + 1')]);
        Graph::where(['user_id' => $file_info->user_id])->
        whereRaw('Date(created_at) = CURDATE()')->
        update(
            [
                'views' => \DB::raw('views + 1'),
                'bandwidth' => \DB::raw('bandwidth + ' . $file_info->encrypted_size)
            ]
        );

    }

}