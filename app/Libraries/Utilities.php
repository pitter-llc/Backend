<?php
/**
 * Created by PhpStorm.
 * User: elycin
 * Date: 10/24/16
 * Time: 2:26 AM
 */

namespace App\Libraries;


use App\LoginNews;
use App\News;
use App\SignupNews;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;

class Utilities
{
    public static function keys_avail()
    {
        return "
        <option value='48'>0 (Number Row)</option>
                                                <option value='49'>1 (Number Row)</option>
                                                <option value='50'>2 (Number Row)</option>
                                                <option value='51'>3 (Number Row)</option>
                                                <option value='52'>4 (Number Row)</option>
                                                <option value='53'>5 (Number Row)</option>
                                                <option value='54'>6 (Number Row)</option>
                                                <option value='55'>7 (Number Row)</option>
                                                <option value='56'>8 (Number Row)</option>
                                                <option value='57'>9 (Number Row)</option>
                                                <option value='96'>0 (Keypad)</option>
                                                <option value='97'>1 (Keypad)</option>
                                                <option value='98'>2 (Keypad)</option>
                                                <option value='99'>3 (Keypad)</option>
                                                <option value='100'>4 (Keypad)</option>
                                                <option value='101'>5 (Keypad)</option>
                                                <option value='102'>6 (Keypad)</option>
                                                <option value='103'>7 (Keypad)</option>
                                                <option value='104'>8 (Keypad)</option>
                                                <option value='105'>9 (Keypad)</option>
                                                <option value='106'>Multiply * (Keypad)</option>
                                                <option value='111'>Divide / (Keypad)</option>
                                                <option value='107'>PLUS + (Keypad)</option>
                                                <option value='109'>MINUS - (Keypad)</option>
                                                <option value='108'>ENTER (Keypad)</option>
                                                <option value='110'>Decimal Point . (Keypad)</option>
                                                <option value='112'>F1</option>
                                                <option value='113'>F2</option>
                                                <option value='114'>F3</option>
                                                <option value='115'>F4</option>
                                                <option value='116'>F5</option>
                                                <option value='117'>F6</option>
                                                <option value='118'>F7</option>
                                                <option value='119'>F8</option>
                                                <option value='120'>F9</option>
                                                <option value='121'>F10</option>
                                                <option value='122'>F11</option>
                                                <option value='123'>F12</option>
                                                <option value='65'>A</option>
                                                <option value='66'>B</option>
                                                <option value='67'>C</option>
                                                <option value='68'>D</option>
                                                <option value='69'>E</option>
                                                <option value='70'>F</option>
                                                <option value='71'>G</option>
                                                <option value='72'>H</option>
                                                <option value='73'>I</option>
                                                <option value='74'>J</option>
                                                <option value='75'>K</option>
                                                <option value='76'>L</option>
                                                <option value='77'>M</option>
                                                <option value='78'>N</option>
                                                <option value='79'>O</option>
                                                <option value='80'>P</option>
                                                <option value='81'>Q</option>
                                                <option value='82'>R</option>
                                                <option value='83'>S</option>
                                                <option value='84'>T</option>
                                                <option value='85'>U</option>
                                                <option value='86'>V</option>
                                                <option value='87'>W</option>
                                                <option value='88'>X</option>
                                                <option value='89'>Y</option>
                                                <option value='90'>Z</option>";
    }

    public function sizeConverter($bytes = 0, $format = "GB", $decimals = 3)
    { //Dynamically set variables if they aren't passed.
        $converted_value = 0;
        switch ($format) {

            case "kb":
            case "KB":
                //Convert to Kilobytes
                $converted_value = ($bytes / 1024);
                break;

            case "mb":
            case "MB":
                //Convert to Megabytes
                $converted_value = ($bytes / 1024 / 1024);
                break;

            case "gb":
            case "GB":
                //Convert to Gigabytes
                $converted_value = ($bytes / 1024 / 1024 / 1024);
                break;

            case "tb":
            case "TB":
                /*
                 * Convert to Terabytes
                 * This should never be used for file uploading, as holy crap we don't have that disk space
                 * but bandwidth maybe.
                */
                $converted_value = ($bytes / 1024 / 1024 / 1024 / 1024);
                break;


        }
        //Return the decimalized formatted value
        return round($converted_value, $decimals);
    }

    public function verificationEmail()
    {
        if (Auth::check()) {
            if (Auth::user()->rank == 0) {
                $str = base64_encode(Crypt::encrypt(Auth::user()->email));
                Mail::raw(
                    "Hello " . ucfirst(Crypt::decrypt(Auth::user()->name)) . ",\n\n" .
                    "We're exited you registered, here's a link to validate your email address for your account:\n" .
                    "https://" . env("DOMAIN", "pitter.us") . "/activate/" . $str . "\n" .
                    "As a security measure, if you account email address fails to be activated within a week from registration it will be terminated.\n\n" .
                    "Happy Uploading,\nThe Pitter Team"
                    , function ($message) {
                    $message->from(env('MAIL_USERNAME'), "Pitter Registration");
                    $message->subject("Pitter Email Address Verification");
                    $message->to(Auth::user()->email);
                });
            }
        }
    }

    public function loginNews()
    {
        $news = News::where('type', 0)->inRandomOrder()->first();
        if (!$news) {
            $news = array(
                "fa" => "fa-exclamation-triangle",
                "title" => "No defined news in database",
                "text" => "There should always be news! Perhaps this is a development build or the database was just rolled back?"
            );
        }
        return $news;
    }

    public function signupNews()
    {
        $news = News::where('type', 1)->inRandomOrder()->first();
        if (!$news) {
            $news = array(
                "fa" => "fa-exclamation-triangle",
                "title" => "Oh dear this isn't good...",
                "text" => "Typically we would display awesome things about our platform here with pictures of it being used, However it looks like our database isn't populated...",
                "background" => "/assets/img/login-bg/whiteroom.jpg"
            );
        }

        return $news;
    }

    public function gen_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

}