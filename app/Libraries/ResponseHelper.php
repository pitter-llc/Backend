<?php
/**
 * Created by PhpStorm.
 * User: elycin
 * Date: 3/12/17
 * Time: 12:00 PM
 */

namespace App\Libraries;


class ResponseHelper
{
    public function successfulDeletion($response_template, $filename)
    {
        $data = $response_template;
        $data["body"] = sprintf($data["body"], $filename);
        $data["filename"] = $filename;
        return $data;
    }

    public function successfulUpload($response_template, $filename)
    {
        $data = $response_template;
        $data["url"] = sprintf("https://i.pitter.us/%s", $filename);
        $data["filename"] = $filename;
        return $data;
    }
}