<?php
/**
 * Created by PhpStorm.
 * User: Elycin
 * Date: 6/4/2016
 * Time: 5:22 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tokens';
}