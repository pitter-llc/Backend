<?php
/**
 * Created by PhpStorm.
 * User: Elycin
 * Date: 6/4/2016
 * Time: 5:22 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $timestamps = false;
    protected $fillable = ["user_id"];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';
}