<?php
/**
 * Created by PhpStorm.
 * User: elycin
 * Date: 10/24/16
 * Time: 2:03 AM
 */

namespace App\Http\Controllers;


use App\Libraries\FileHandler;
use App\Libraries\Pitter;

class APIController extends Controller
{
    function getUploadResponses()
    {
        return response(json_encode(FileHandler::$RESPONSES));
    }

    public function getFileCategories()
    {
        return response(json_encode(Pitter::$DEFINITIONS));
    }
}