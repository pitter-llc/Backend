<?php
/**
 * Created by PhpStorm.
 * User: elycin
 * Date: 11/18/16
 * Time: 3:14 AM
 */

namespace App\Http\Controllers;


use App\Libraries\ClamAV;
use App\Libraries\FileHandler;
use App\Libraries\Statistics;
use App\Libraries\ThumbnailHandler;
use App\Libraries\Utilities;
use App\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    private $temp_dir = "temporary";
    private $statistics;
    private $handler;
    private $clamav;
    private $utilities;

    public function __construct()
    {
        $this->statistics = new Statistics();
        $this->handler = new FileHandler();
        $this->clamav = new ClamAV();
        $this->utilities = new Utilities();
    }

    public function upload(Request $request)
    {
        if (!$request->hasFile("file")) return response(json_encode(FileHandler::$RESPONSES["FileNotPassedInPostRequest"]));

        $this->handler->authCheck($request);

        $temporary_filename = sprintf("%s.%s", $this->utilities->gen_uuid(), $request->file("file")->getClientOriginalExtension());
        $request->file("file")->storeAs($this->temp_dir, $temporary_filename);

        if ($this->clamav->isFileVirus(storage_path(sprintf("%s/%s", $this->temp_dir, $temporary_filename)))) {
            Storage::delete($temporary_filename);
            return response(json_encode(FileHandler::$RESPONSES["MalwareDetected"]));
        }

        /*if (User::isSuspended()) {
            return response(json_encode(FileHandler::$RESPONSES["AccountSuspended"]));
        }
         * */

        $DATA_STREAM = Storage::get(sprintf("%s/%s", $this->temp_dir, $temporary_filename));
        $DATA_STREAM_SIZE = strlen($DATA_STREAM);

        if (boolval($request->input("encrypt"))) {
            $DATA_STREAM = Crypt::encrypt($DATA_STREAM);
            $DATA_STREAM_ENCRYPTED_SIZE = strlen($DATA_STREAM);
        } else {
            /*
             * Make equal values
             * In the database a sum calculation is performed of encrypted sizes.
             * If the user doesn't want to encrypt the file we can simply discard encrypted values by setting them to the DATA_STREAM values
             * */
            $DATA_STREAM_ENCRYPTED_SIZE = $DATA_STREAM_SIZE;
        }

        $response = $this->handler->upload($request->file("file")->getClientOriginalExtension(), $DATA_STREAM);

        if ($response["type"] == "success") {
            $new = new Upload();

            $new->user_id = $request->user()->id;
            $new->filename = $response["filename"];
            $new->original_filename = $request->file("file")->getClientOriginalName();
            $new->size = $DATA_STREAM_SIZE;
            $new->encrypted_size = $DATA_STREAM_ENCRYPTED_SIZE;
            $new->mime = $request->file("file")->getClientMimeType();
            $new->encrypted = boolval($request->input("encrypt"));

            $new->save();

            $this->statistics->logUpload(strlen($DATA_STREAM));

            return response(json_encode($response), 201);
        } else {
            return response(json_encode($response));
        }
    }

    public function delete(Request $request)
    {
        $this->handler->authCheck($request);
        $response = $this->handler->delete($request);
        if ($response["type"] == "success") {
            Upload::where('filename', $request->input("filename"))->delete();
        }
        return $response;
    }

    public function retrieve($filename)
    {
        $file_info = Upload::where("filename", $filename)->firstOrFail();
        $this->statistics->logView($file_info);
        return $this->handler->download($file_info);
    }

    public function thumbnail($filename)
    {
        $file_info = Upload::where("filename", $filename)->firstOrFail();
        $thumbnailEngine = new ThumbnailHandler($file_info);

        if (!$thumbnailEngine->exists()) {
            $thumbnailEngine->createFromCacheInstance($this->handler->FileCache);
        }

        return $thumbnailEngine->getThumbnail();
    }
}