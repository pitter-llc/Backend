<?php
/**
 * Created by PhpStorm.
 * User: elycin
 * Date: 10/24/16
 * Time: 2:03 AM
 */

namespace App\Http\Controllers;


use App\APIKey;
use App\Libraries\ClamAV;
use App\Libraries\NFS;
use App\Libraries\Pitter;
use App\Libraries\Statistics;
use App\Libraries\Utilities;
use App\Setting;
use App\User;
use Illuminate\Support\Facades\Auth;

class PanelController extends Controller
{
    private $pitter;
    private $statistics;
    private $utilities;
    private $clamav;

    public function __construct()
    {
        $this->pitter = new Pitter();
        $this->statistics = new Statistics();
        $this->utilities = new Utilities();
        $this->clamav = new ClamAV();
        $this->middleware('auth');
    }

    public function dashboard()
    {
        //automatically make sure we have a entry for today - there should just be a dot at [0,0]
        $this->statistics->userGraphTemplate(Auth::user()->id);

        //Okay, we can display the panel now.
        return view("panel/home", $this->statistics->dashboard(30));
    }

    public function administration()
    {
        return view("panel/administration");
    }

    public function developer()
    {
        return view("panel/developer");
    }

    public function settings()
    {

        Setting::updateOrCreate(
            ["user_id" => Auth::user()->id]
        );

        return view("panel/settings",
            [
                "name" => User::getName(),
                "email" => User::getEmail(),
                "rank" => User::getRank(),
                "rank_title" => User::getRankTitle(),
                "premium" => User::isPremium(),
            ]
        );
    }

    public function uploads()
    {
        return view("panel/uploads", [
            "categories" => $this->pitter->getFilesFancy()
        ]);
    }

    public function dashboardStatistics($days)
    {
        return $this->statistics->dashboard(intval($days));
    }

    public function workshop()
    {
        return view("panel/workshop");
    }

    public function termsOfService()
    {
        return view("panel/information/terms-of-service");
    }
}