<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Libraries\Utilities;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request)
    {
        $utilities = new Utilities();
        if (!$request->has("name")) {
            return view("auth/register", [
                "error" => "Please provide a name"
            ]);
        }
        if (!$request->has("email")) {
            return view("auth/register", [
                "error" => "Please provide an email address"
            ]);
        }
        if (!$request->has("password")) {
            return view("auth/register", [
                "error" => "Please provide a valid password"
            ]);
        }
        if (strpos($request->input("email"), '@') === false || strpos($request->input("email"), '.') === false || strlen($request->input("email")) < 4) {
            return view("auth/register", [
                "error" => "Invalid email address"
            ]);
        }
        if (strlen($request->input("password")) < 6) {
            return view("auth/register", [
                "error" => "Please enter a password longer than 6 characters"
            ]);
        }
        if (!$request->has("agree")) {
            return view("auth/register", [
                "error" => "You did not agree to the Terms of Service"
            ]);
        }

        $new = new User();
        $new->name = Crypt::encrypt($request->input("name"));
        $new->email = $request->input("email");
        $new->password = bcrypt($request->input("password"));
        $new->save();

        //Login the new user
        Auth::loginUsingID($new->id);

        //Send Verification Email
        $utilities->verificationEmail();

        return redirect("/");

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
