<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Libraries\Utilities;
use App\Token;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $utilities;

    public function __construct()
    {
        $this->utilities = new Utilities();
        $this->middleware('guest', ['except' => 'logout']);

    }

    public function login(Request $request)
    {
        if (!$request->has("email")) {
            return view("auth/login", [
                "news" => $this->utilities->loginNews(),
                "error" => "Please provide a valid email address."
            ]);
        } else {
            if (!$request->has("password")) {
                return view("auth/login", [
                    "news" => $this->utilities->loginNews(),
                    "email" => $request->input("email"),
                    "error" => "Please provide a password."
                ]);
            } else {
                if (Auth::attempt([
                    "email" => $request->input("email"),
                    "password" => $request->input("password")
                ], $request->has("remember"))
                ) {
                    if (Auth::user()->rank == -1) {
                        Auth::logout();
                        return view("auth/login", [
                            "news" => $this->utilities->loginNews(),
                            "error" => "Account has been suspended"
                        ]);
                    } else {
                        try{
                            User::where('id', Auth::user()->id)
                                ->update(["ip" => Crypt::encrypt($_SERVER["HTTP_CF_CONNECTING_IP"])]);
                        } catch (\Exception $exception){
                            User::where('id', Auth::user()->id)
                                ->update(["ip" => Crypt::encrypt($_SERVER["REMOTE_ADDR"])]);
                        }
                        return redirect("/");
                    }
                } else {
                    return view("auth/login", [
                        "news" => $this->utilities->loginNews(),
                        "email" => $request->input("email"),
                        "error" => "Invalid email address or password."
                    ]);
                }
            }
        }
    }

    public function clientLogin(Request $request)
    {
        if (!$request->has("email")) {
            return view("auth/login", [
                "news" => $this->utilities->loginNews(),
                "error" => "Please provide a valid email address."
            ]);
        } else {
            if (!$request->has("password")) {
                return view("auth/login", [
                    "news" => $this->utilities->loginNews(),
                    "email" => $request->input("email"),
                    "error" => "Please provide a password."
                ]);
            } else {
                if ($_SERVER['HTTP_USER_AGENT'] != "PitterClient/1.0/" . $request->input("email")) {
                    return view("errors/503", [
                        "fa" => "fa-exclamation-triangle",
                        "h1" => "Client Login Failure",
                        "message" => "Pitter was unable to verify the integrity of the client you are using."
                    ]);
                } else {
                    if (Auth::attempt([
                        "email" => $request->input("email"),
                        "password" => $request->input("password")
                    ], 0)
                    ) {
                        User::where('id', Auth::user()->id)
                            ->update(["ip" => Crypt::encrypt($_SERVER["HTTP_CF_CONNECTING_IP"])]);
                        return json_encode([
                            "status" => "success",
                            "token" => $this->generateToken(),
                            "name" => Crypt::decrypt(Auth::user()->name)
                        ]);
                    } else {
                        return json_encode(["status" => "error", "message" => "Invalid Email or Password"]);
                    }
                }
            }
        }
    }

    public function generateToken()
    {
        $token_info = Token::where('user_id', Auth::user()->id)->first();
        if (!$token_info) {
            $token_info = new Token;
            $token_info->user_id = Auth::user()->id;
            $token_info->token = $this->tokenStringGenerator();
            $token_info->save();
            return $token_info->token;
        } else {
            $token_info->delete();
            return $this->generateToken();
        }
    }

    public function tokenStringGenerator()
    {
        $random_string = "";
        for ($i = 0; $i <= 10; $i++) {
            $random_string .= uniqid();
        }
        return $random_string;
    }

    public function tokenLogin($token)
    {
        $token_info = Token::where("token", $token)->first();
        if (!$token_info) {
            return redirect("/login");
        } else {
            Auth::loginUsingId($token_info->user_id);
            $token_info->delete();
            return redirect("/");
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect("/");
    }
}
