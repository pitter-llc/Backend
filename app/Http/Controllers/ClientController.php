<?php
/**
 * Created by PhpStorm.
 * User: elycin
 * Date: 11/19/16
 * Time: 4:09 PM
 */

namespace App\Http\Controllers;


use App\Setting;
use App\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{

    public function __construct()
    {

    }

    public function settings(Request $request)
    {
        $this->checkAuth($request);
        return json_encode(Setting::where('user_id', Auth::user()->id)->first());
    }

    public function checkAuth(Request $request)
    {
        if (!Auth::attempt(["email" => $request->input("email"), "password" => $request->input("password")], 0)) {
            return response()->view('errors.custom', [
                "fa" => "fa-exclamation-triangle",
                "h1" => "Invalid Credentials",
                "message" => ""
            ], 500);
        }
    }

    public function files(Request $request)
    {
        $this->checkAuth($request);
        return json_encode(Upload::where('user_id', Auth::user()->id)->select(["filename", "original_filename", "size"])->get());
    }


}