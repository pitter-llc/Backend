<?php

namespace App\Http\Controllers;

use App\Libraries\Statistics;
use App\Libraries\Utilities;
use App\Payment;
use App\User;
use Fahim\PaypalIPN\PaypalIPNListener;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;


class Controller extends \Illuminate\Routing\Controller
{
    private $statistics;
    private $utilities;

    public function __construct()
    {
        $this->statistics = new Statistics();
        $this->utilities = new Utilities();
    }

    public function homepage()
    {
        return view("homepage/index", $this->statistics->homepage());
    }

    public function abuse()
    {
        return view("homepage/abuse");
    }

    public function contact()
    {
        return view("homepage/contact");
    }

    public function loginView()
    {
        if (!Auth::check()) {
            return view("auth/login", [
                "news" => $this->utilities->loginNews()
            ]);
        } else {
            return redirect("/");
        }
    }

    public function registerView()
    {
        if (!Auth::check()) {
            return view("auth/register", [
                "news" => $this->utilities->signupNews()
            ]);
        } else {
            return redirect("/");
        }

    }

    public function termsofservice()
    {
        return view("homepage/terms");
    }

    public function datapolicy()
    {
        return view("homepage/data");
    }

    public function cookiepolicy()
    {
        return view("homepage/cookie");
    }

    public function homepageStatistics()
    {
        return json_encode($this->statistics->homepage());
    }

    public function emailVerify($token)
    {
        try {
            $email = Crypt::decrypt(base64_decode($token));
            if (User::where([
                ['email', $email],
                ['rank', 0]
            ])->first()
            ) {
                User::where('email', $email)->update(["rank" => 1]);
                return view("email/verified");
            } else {
                return view("email/failed");
            }
        } catch (\Exception $e) {
            return view("email/failed");
        }

    }


    public function IPN(Request $request)
    {
        $ipn = new PaypalIPNListener();

        //Sandbox
        $ipn->use_sandbox = false;

        //Process
        $verified = $ipn->processIpn();

        if ($verified) {
            switch ($request->input("payment_status")) {
                case "Refunded":
                case "Reversed":
                    /*
                     * Reverse Payment
                     */
                    $transaction = Payment::where("transaction", $request->input("txn_id"))->first();
                    if ($transaction) {
                        User::where('user_id', $transaction->user_id)->update(['rank' => '1']);
                    }
                    break;
                default: {
                    /*
                     * Successful Payment Response
                     * */
                    if ($request->has("custom")) { //The email to validate.

                        //Grab the user
                        $user = User::where('email', strtolower(trim($request->input("custom"))))->first();

                        $payment = new Payment;
                        $payment->user_id = $user->id;
                        $payment->transaction = $request->input("txn_id");
                        $payment->payer = $request->input("payer_email");
                        $payment->amount = $request->input("mc_gross");
                        $payment->save();

                        //Change the user's rank
                        $user->rank = 2;
                        $user->save();
                    }
                    break;
                }
            }
        }
    }
}
