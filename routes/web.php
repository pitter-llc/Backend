<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Group definitions

$home = function () {
    Route::get('/', 'Controller@homepage');
    Route::get('/contact', 'Controller@contact');
    Route::get('/abuse', 'Controller@abuse');
    Route::get('/terms', 'Controller@termsofservice');
    Route::get('/data', 'Controller@datapolicy');
    Route::get('/cookies', 'Controller@cookiepolicy');
    Route::get('/statistics', 'Controller@homepageStatistics');

    //Email Activation
    Route::get('/activate/{token}', 'Controller@emailVerify');

    //Paypal
    Route::post('/ipn', 'Controller@IPN');
};
$panel = function () {

    //Authentication
    Route::get('/login', 'Controller@loginView');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::get('/register', 'Controller@registerView');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/register', 'Auth\RegisterController@register');


    //Management
    Route::get('/account/resend-verification-email', 'PanelController@resendVerificationEmail');
    Route::get('/account/deactivate', 'PanelController@deleteUserAccount');

    //Dashboard
    Route::get('/', 'PanelController@dashboard');


    //Uploads
    Route::get('/uploads', 'PanelController@uploads');


    //Developer
    Route::get('/developer', 'PanelController@developer');
    Route::get('/developer/profile/{developer}', function () {
    });


    //Workshop
    Route::get('/workshop', 'PanelController@workshop');
    Route::get('/workshop/application/{application_id}', function () {
    });
    Route::get('/workshop/application/{application_id}/download', function () {
    });


    //Statistics
    Route::get('/statistics/{days}', 'PanelController@dashboardStatistics');


    //Information
    Route::get('/help/terms-of-service', 'PanelController@termsOfService');


};

//Actual domain routing
Route::group(['domain' => 'localhost'], $home);
Route::group(['domain' => 'pitter.us'], $home);

Route::group(['domain' => 'panel.localhost'], $panel);
Route::group(['domain' => 'panel.pitter.us'], $panel);


//Catch all
Route::get('/{filename}', 'UploadController@retrieve');
Route::get('/thumb/{filename}', 'UploadController@thumbnail');