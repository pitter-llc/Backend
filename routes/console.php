<?php

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/


Artisan::command('lookup {email}', function ($email) {
    $user = \App\User::where('email', $email)->first();
    $data = json_decode($user, true);
    $data["name"] = \Illuminate\Support\Facades\Crypt::decrypt($data["name"]);
    $data["ip"] = \Illuminate\Support\Facades\Crypt::decrypt($data["ip"]);
    echo var_dump($data);
});

Artisan::command('datadump {action}', function ($action) {
    switch ($action) {
        case "backup":
            File::put('users.database', json_encode(\App\User::all()));
            File::put('uploads.database', json_encode(\App\Upload::all()));
            break;
        case "restore":
            $json = json_decode(File::get('users.database'));
            foreach ($json as $item) {
                $new = new \App\User();
                $new->id = $item->id;
                $new->name = $item->name;
                $new->email = $item->email;
                $new->password = $item->password;
                $new->rank = $item->rank;
                echo sprintf("%s %s %s\n", "Importing user", $item->email, "back into the database...");
                $new->save();
            }

            $json = json_decode(File::get('uploads.database'));
            foreach ($json as $item) {
                $new = new \App\Upload();
                $new->id = $item->id;
                $new->user_id = $item->user_id;
                $new->filename = $item->filename;
                $new->original_filename = $item->original_filename;
                $new->rank = $item->rank;
                echo sprintf("%s %s %s\n", "Importing upload", $item->email, "back into the database...");
                $new->save();
            }

            break;
        default:
            echo "Unknown Action\n";
    }
});
