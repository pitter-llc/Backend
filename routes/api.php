<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Upload functions
Route::post('/upload', 'UploadController@upload');
Route::post('/delete', function () {
});


//Other
Route::post('/login-request', function () {
});


//Information
Route::get('/information/responses/upload', 'APIController@getUploadResponses');
Route::get('/information/responses/file-categories', 'APIController@getFileCategories');
