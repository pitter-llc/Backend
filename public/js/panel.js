/**
 * Created by elycin on 1/8/17.
 */

var Core = {
    dashboard: {
        updateFrequency: 5000,
        registerGraph: function () {
            $.getJSON("/statistics/30", function (data) {
                var chartContext = document.getElementById('month').getContext('2d')
                Core.dashboard.chart = new Chart(chartContext, {
                    type: 'line',
                    options: {
                        responsive: true,
                        maintainAspectRatio: false
                    },
                    data: data.graph
                });
            });
        },
        initialize: function () {
            if (window.location.pathname == "/" || window.location.pathname == "/dashboard") {
                this.registerGraph();
                this.graphLoop = setInterval(function () {
                    Core.statistics.dashboardStats();
                }, Core.dashboard.updateFrequency);
            }
        }
    },
    gallery: {
        deleteMode: false,
        initialize: function () {

        }
    },
    settings: {
        initialize: function () {

        }
    },
    dropper: {
        uploading: false,
        upload: function () {

        },
        initialize: function () {

        }
    },
    utilities: {
        arrayIsIdentical: function (arr1, arr2) {
            if (arr1.length !== arr2.length)
                return false;
            for (var i = arr1.length; i--;) {
                if (arr1[i] !== arr2[i])
                    return false;
            }
            return true;
        },
        numberCompare: function (o, n) {
            var diff = parseInt(n) - parseInt(o);
            return "+" + diff.toString();
        },
        initialize: function () {

        }
    },
    statistics: {
        dashboardStatsTimePeriod: 30,
        dashboardStats: function () {
            $.getJSON("/statistics/" + this.dashboardStatsTimePeriod, function (data) {

                //Comapare the last graph data
                var last = [
                    Core.dashboard.chart.data.datasets[0].data[Core.statistics.dashboardStatsTimePeriod - 1],
                    Core.dashboard.chart.data.datasets[1].data[Core.statistics.dashboardStatsTimePeriod - 1]
                ];

                var downloaded = [
                    data.graph.datasets[0].data[Core.statistics.dashboardStatsTimePeriod - 1],
                    data.graph.datasets[1].data[Core.statistics.dashboardStatsTimePeriod - 1],
                ];

                if (!Core.utilities.arrayIsIdentical(last, downloaded)) {
                    Core.dashboard.chart.data.datasets[0].data[Core.statistics.dashboardStatsTimePeriod - 1] = data.graph.datasets[0].data[Core.statistics.dashboardStatsTimePeriod - 1];
                    Core.dashboard.chart.data.datasets[1].data[Core.statistics.dashboardStatsTimePeriod - 1] = data.graph.datasets[1].data[Core.statistics.dashboardStatsTimePeriod - 1];
                    Core.dashboard.chart.update();
                    console.log("[" + Date() + "] Graph index " + (Core.statistics.dashboardStatsTimePeriod - 1) +
                        " has been updated - [" +
                        Core.utilities.numberCompare(
                            last[0], downloaded[0]
                        ) + ", " +
                        Core.utilities.numberCompare(
                            last[1], downloaded[1]
                        ) + "]"
                    );
                }

                //Other element update
                //Total or Current
                $("#total-files").html(data.total.files);
                $("#total-views").html(data.total.views);
                $("#total-storage").html(data.total.space.formatted);
                $("#total-bandwidth").html(data.total.bandwidth.formatted);

                //Monthly
                $("#month-files").html(data.timed.files);
                $("#month-views").html(data.timed.views);
                $("#month-storage").html(data.timed.space.formatted);
                $("#month-bandwidth").html(data.timed.bandwidth.formatted);
            });
        },
        initialize: function () {
            // Do nothing!
        }
    },
};

$(document).ready(function () {
    $.each(Core, function (key, value) {
        Core[key].initialize();
    });
});