/**
 * Created by elycin on 3/18/17.
 */
toastr.options = {
    "closeButton": true,
    "debug": false,
    "progressBar": true,
    "preventDuplicates": true,
    "positionClass": "toast-bottom-center",
    "onclick": null,
    "showDuration": "400",
    "hideDuration": "1000",
    "timeOut": "7000",
    "extendedTimeOut": "4000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

var deleteModeSetting = false;
var isUploadingSetting = false;
var isDragAndDroppingSetting = false;
var deleteModeCheckBoxElement = "#toggle-delete-mode";
var uploadedObjectClass;
var dragAndDropObjectClass = ".drag-and-drop";
const concurrentFileUploadsMax = 3;
var concurrentFileUploads = 0;
var failedUploads = 0;
const fileSizeHardLimit = 100 * (1024 ** 2);


function notificationToastr(json) {
    /*
     * Notification Parser
     * This function will parse responses and display them to the user from the backend.
     * */
    eval("toastr." + json["type"] + "('" + json["title"] + "', '" + json["body"] + "')");
}

$(deleteModeCheckBoxElement).change(function () {
    deleteModeSetting = $(this).is(":checked");
    switch (deleteModeSetting) {
        case true: {
            /*
             * Delete Mode Enabled
             * Notify the user that delete mode is active, and that whatever they click will be permanently deleted
             * */
            toastr.error("Be careful on what you choose to delete, there's no way back...", "Delete Mode Enabled");
            break;
        }
        case false: {
            /*
             * Delete Mode Disabled
             * Notify the user that it is safe to click things.
             * */
            toastr.info("It is now safe to click on uploads", "Delete Mode Disabled");
            break;
        }
    }
});

function deleteClickedObject() {
    const clickedFilename = $(this).attr('filename');
    /*
     * Delete Post Request
     * This post request will contact the API and the backend will do the rest for deletion.
     * This should be an ajax request for full control of the functions.
     * */
    $.ajax({
        url: "/api/delete",
        type: "POST",
        async: true, //Code should always be asynchronous
        cache: false, //Do not cache - responses may change.
        dataType: "json", //Signal that we're working with JSON
        data: {
            filename: clickedFilename
        },
        complete: function (xhr, status) { //What to do when complete with the request.
            notificationToastr(xhr); //Make a notification
            $(this).remove(); //Delete the element
        },
        error: function (xhr, status, error) { //What to do when the request fails.
            console.error([xhr, status, error]);
        }
    })
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function createUploadNotificationLayout() {
    var GUID = guid();
    var template = '<li><div class="small pull-right m-t-xs" id="' + GUID + '-right"></div><h4 id="' + GUID + '-header"></h4><span id="' + GUID + '-text"></span><br></li>';
    $("#drag-and-drop-list").append(template);
    return GUID;
}

function progressBarGenerator(value) {
    return '<div class="progress progress-small"><div style="width: ' + value.toString() + '%;" class="progress-bar"></div></div>';
}

function uploadFile(item) {
    if (item.size < fileSizeHardLimit) {
        var formData = new FormData();
        formData.append("file", item);

        const notificationIdentification = createUploadNotificationLayout();

        var request = new XMLHttpRequest();
        request.open('POST', '/api/upload');
        request.onloadstart = function (e) {
            concurrentFileUploads++;
            $("#" + notificationIdentification + "-header").html(item.name);
            if (item.size > 1024000) {
                $("#" + notificationIdentification + "-right").html((item.size / 1024 / 1024).toFixed(2).toString() + " MiB");
            } else {
                $("#" + notificationIdentification + "-right").html((item.size / 1024).toFixed(2).toString() + " KiB");
            }
            $("#" + notificationIdentification + "-text").html("Establishing connection to the server");
        };
        request.onprogress = function (e) {
            $("#" + notificationIdentification + "-text").html(progressBarGenerator((event.loaded / event.total) * 100));
        };
        request.onload = function (e) { //Complete
            concurrentFileUploads--;
            console.log(e.srcElement.response);
            switch (request.status) {
                case 404:
                case 500:
                    $("#" + notificationIdentification + "-text").html("Service Error - File Discarded");
                    failedUploads++;
                    if (concurrentFileUploads == 0) {
                        if (failedUploads != 0) {
                            if (failedUploads == 1) {
                                toastr.error("One file has been discarded due to a service issue", "Files Discarded");
                            } else {
                                toastr.error("A total of " + failedUploads + " files failed to upload due to a service issue", "Files Discarded");
                            }
                            failedUploads = 0;
                        }
                    }
                    break;
                case 200:
                    $("#" + notificationIdentification + "-text").html("Odd Response - See Console");
                    break;
                case 201:
                    $("#" + notificationIdentification + "-text").html("Upload Successful");
                    break
            }
        };
        request.send(formData);
    } else {
        toastr.error("The file " + item.name + " was discarded due to it's size exceeding " + (fileSizeHardLimit / 1024 / 1024).toFixed(0).toString() + " MiB", "File Too Large");
    }
}

function dragAndDropProcessor(object) {
    $(object).on('dragover', function (event) {
        event.stopPropagation();
        event.preventDefault();
    });
    $(object).on('drop', function (event) {
        event.preventDefault();
        console.log(event.originalEvent.dataTransfer.files);
        $.each(event.originalEvent.dataTransfer.files, function (index, value) {
            uploadFile(value);
        });
    });
}

$(document).ready(function () {
    $(dragAndDropObjectClass).each(function () {
        dragAndDropProcessor(this);
    });
});
